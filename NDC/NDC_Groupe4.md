#### NF17 - TD G3

# Note de clarification du groupe 4

##### Sujet : Championnats de football

##### Objectif : ​ Réaliser une application qui permet de gérer informatiquement un championnat de football.

Projet réalisé par :

- Clément MOUCHET
- Florent GAUDIN
- Xuefeng HU
- Zilu ZHENG
- Rémi BESOIN

git : https://gitlab.utc.fr/besoinre/projet_nf17_p19_tdg3_groupe

<br>

## Demande du client

L'objectif de cette application est de développer une application permettant aux administrateurs d'un championnat de football de gérer informatiquement ce dernier. L'application devra aussi permettre aux utilisateurs de pouvoir consulter les informations d'un championnat. A partir des données saisies, l'état du championnat s'actualisera automatiquement. 

<br>

## Données du site

Les joueurs, les entraineurs et les arbitres sont des personnes caractérisées de la manière suivante:

Les joueurs ont les attributs suivants:
<br>
**Joueur :**
- nom
- prénom 
- surnom
- date de naissance
- poste :
    - gardien
    - défenseur
    - milieu
    - attaquant
- statut :
    - titulaire
    - remplaçant

    
_Le nom, prénom, surnom et la date de naissance d'un joueur doivent toujours être renseigné (NOT NULL)._

_Un joueur ne peut pas faire partie de plusieurs équipes._

<br>

**Entraineur :**
- nom
- prenom
- date de naissance
- équipe associée

_Le nom, prénom et la date de naissance d'un entraineur doivent toujours être renseigné (NOT NULL)._

_Un entraineur peut être un joueur._

<br>

Les différentes équipes qui composent la base de donnée sont caractérisées par:

**Équipe :**
- nom -> doit être unique
- surnoms -> une équipe peut avoir plusieurs surnoms
- ville -> une équipe appartient à une seule ville
- joueurs -> 8 joueurs minimum
- entraîneur -> peut être l'un des joueurs de l'équipe

_Le nom de l'équipe et la ville doivent être renseignés. Le nom de l'équipe est unique._

_Une équipe peut ne pas faire partie d'un championnat._

**Ville :**
- Nom de la ville
- Pays
- Nombre de stades
- Population

_Le nom de la ville et le pays doivent être renseignés._

_Une ville peut avoir plusieurs équipes._ 

**Match :**
- équipe locale 
- équipe visiteur
- 11 joueurs titulaires-> au maximum par équipe (nombre de remplaçants illimités)
- Durée: 90 minutes (2 mi-temps de 45 minutes)
- Date du match
- Localisation du match (ville)
- 4 arbitres (1 central, 2 assistants, 1 remplaçant)
- Evenements du match (but, sanction, changement de joueur)
- Score associé

_Les 2 équipes, la date, les arbitres doivent toujours être renseignées (NOT NULL)._

**Arbitre :**
<br>
- nom
- prenom
- L'arbitre peut sanctionner des joueurs (événements)

_Les arbitres et les joueurs ne sont pas confondus. Le rôle de l'arbitre peut changer à chaque match._

_Le nom et prénom doivent être renseignés (NOT NULL)._

<br>

Les matchs sont caractérisés par des événements de 3 types:

**Sanction :** 
- joueur sanctionné
- type de sanction (avertissement, une expulsion directe ou une expulsion suite à un double avertissement.)
- date de la sanction
- arbitre (qui a sanctionné)
- Match concerné


_Toutes les informations doivent être renseignées._

<br>

**But :**
- auteur du but
- auteur de la passe
- date du but
- Match concerné

_Le score du match sera mis automatiquement à jour._

_Le nom du buteur, la date du but et le match concerné doivent être renseignés (NOT NULL). Le nom du passeur est facultatif et différent du nom du buteur._

<br>

**Changement de joueur:**
- joueur sortant
- joueur entrant
- date du changement
- Match concerné

_Toutes les informations doivent être renseignées._

<br>

_Lors d'un changement, le joueur passe de titulaire à remplaçant, et vice versa._

**Score :** 
- équipe locale
- équipe visiteur
- match concerné

_Toutes les informations doivent être renseignées._

_L'équipe gagnante est celle avec le meilleur score._

_À chaque événement "But" le score doit être actualisé._ 

<br>

**Championnat :**
- nom du championnat
- date de début
- date de fin
- liste des équipes (nombre pair)
- matchs -> plusieurs
- classement
- nom de ses sponsors

_Le nom, la date de début et la date de fin doivent être renseignés._

_Une équipe qui n'est pas dans le championnat ne peut pas être classée._

*À l’issue des matchs, l'équipe gagnante est celle qui finit première du classement, c’est-à-dire avec le nombre de matchs gagnés le plus élevé.*

<br>

**Classement :**

- nom des équipes 
- nombre de points par équipe

_Au début du championnat, toutes les équipes ont 0 points._

_Toutes les informations doivent être renseignées._

A l'issue de chaque match, le classement sera automatiquement mis à jour. Le nombre de points des équipes concernés évoluent en fonction du score.

- 3 points pour le gagnant
- 1 point pour chaque équipe pour un match nul
- 0 point pour le perdant



<br><br><br>

## Fonctionnalités du site

#### Niveau 1

L’administrateur du site doit pouvoir **ajouter, modifier et supprimer**  :
- des joueurs
- des entraîneurs
- des arbitres
- des équipes
- des championnats
- des matchs dans un championnat
- les événements d’un match

<br>

Les utilisateurs du site doivent pouvoir **visualiser** :
- le classement actuel des équipes dans le championnat avec les détails
- le classement des meilleurs buteurs du championnat
  - possibilité de restreindre à une période précise
- le classement des meilleurs passeurs du championnat
  - possibilité de restreindre à une période précise
- les équipes les plus fortes à domicile
- les équipes les plus fortes à l’extérieur

<br>

#### Niveau 2


Les utilisateurs du site doivent pouvoir **visualiser** :

- les données de chaque équipe
  - nombre de buts de chaque équipe
  - les points des équipes
  - le nombre de matchs gagnés
  - le nombre de matchs ex aequo
  - le nombre de matchs perdus
  - la différence de but 
  - les données de chaque joueur dans l'équipe
    - le nombre de fois où il a été sanctionné
    - le nombre de buts qu’il a marqué
    - le nombre de passes décisives
- Arbitres
  - le nombre de sanction donné par un arbitre
  - le nombre de fois ou l'arbitre occupe un certain rôle

- Match
  - nombre de match dans une ville
  - le match avec le plus d'événements

<br>

_Remarque: l’administrateur doit être libre de réaliser d’autres opérations s’il le souhaite._

_Remarque: les utilisateurs doivent être libre de réaliser d’autres opérations de consultation s’il le
souhaite._




