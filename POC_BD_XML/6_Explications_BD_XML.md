# Note explicative sur ce qu'on a gagné et perdu d'un modèle relationnel à un modèle XML

Passage vers un modèle XML 

(le 09.06.2019) 

Le méta-langage XML sert à définir des formats informatiques. Les bonnes caractéristiques d'XML - non-ambiguïté, lisibilité, passivité - en font un très bon candidat pour de 
très nombreux usages, il est donc utilisé dans des secteurs très variés de l'informatique. Il permet d'échanger aisément des données entre base de données.
Une base de données incluant le format XML permet de définir des structures de données complexes personnalisables grâce à l'utilisation des balises. Il est possible de 
créer des tables composées uniquement d'élément XML ou de faire appel à XML pour certains types d'attributs.
Concernant notre projet "Championnat de football", nous avons sélectionner une partie de notre MCD qui est adaptée à l'utilisation du format XML, cette MCD fait appel à :
- des compositions : staff compose équipe, 
- des data types : pays et organisaterur,
- des attributs  : surnoms.

Le format XML permet d'apporter une souplesse au niveau du format des données des attributs de nos tables puisque nous pouvons déterminer les balises à utiliser et ces balises 
peuvent varier en fonctions des éléments de la base de données. Enfin, XML permet de contrôler les données grâce à l'utilisation de schéma XML.
