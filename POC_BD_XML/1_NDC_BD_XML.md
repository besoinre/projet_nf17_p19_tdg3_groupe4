# Modification Cahier des charges et Note de clarification

*(le 26.05.2019)* <br><br>

Une équipe est composée d'un staff. Les membres de ce staff sont l'entraineur principal, l'entraineur des gardiens, le médecin, les kinés et les cuisiniers.

Un pays est décrit par son nom ainsi que son code pays composé de deux lettres (Ex: France, FR).

L'organisateur du championnat est décrit par son nom, il s'agit soit d'une association soit d'une entreprise.
