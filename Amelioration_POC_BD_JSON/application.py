

#!/usr/bin/python3
import psycopg2



# Configuration de la BDD -----------------------------------------------------------------------------------------

HOST = "tuxa.sme.utc"
USER = "bdd0p060"
PASSWORD = "cAW2grC4"
DATABASE = "dbbdd0p060"

# Open connection
conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))

# Open a cursor to send SQL commands
cur = conn.cursor()




# Les routines d'executions de l'application -----------------------------------------------------------------------

# Les insert **********************************************************************

def insertionVille():
    print("\n\nInsertion d'une ville:")
    print("-------------------------")
    nom_ville = input("Nom: ")
    nom_pays = input("Pays: ")
    code_pays = input("Code Pays: ")
    nb_stades = input("Nombre de stades: ")
    population = input("Population: ")

    sql = "INSERT INTO Ville VALUES ( \'" + nom_ville + "\', \'{\"pays\":\"" + nom_pays + "\", \"code\":\"" + code_pays + "\"}\', " + nb_stades + "," + population + ");"
    cur.execute(sql)
    conn.commit()

    print("\n>> Insertion ville: OK")


def insertionChampionnat():
    print("\n\nInsertion d'un championnat:")
    print("-------------------------------")
    nom = input("Nom: ")
    dateDebut = input("Date debut (MM/DD/YYYY): ")
    dateFin = input("Date fin (MM/DD/YYYY): ")
    nom_organsisateur = input("Organisateur: ")
    type_organisateur = int(input("Est-ce une entreprise ? (1-oui 0-non) "))

    if(type_organisateur == 1):
        sql = "INSERT INTO Championnat VALUES (\'" + nom + "\',TO_DATE(\'" + dateDebut + "\', \'MM/DD/YYYY\'), TO_DATE(\'" + dateFin + "\', \'MM/DD/YYYY\'), \'{\"nom\":\"" + nom_organsisateur + "\", \"association\":\"FALSE\", \"entreprise\":\"TRUE\"}\');"
        cur.execute(sql)
        conn.commit()
    elif(type_organisateur == 0):
        sql = "INSERT INTO Championnat VALUES (\'" + nom + "\',TO_DATE(\'" + dateDebut + "\', \'MM/DD/YYYY\'), TO_DATE(\'" + dateFin + "\', \'MM/DD/YYYY\'), \'{\"nom\":\"" + nom_organsisateur + "\", \"association\":\"TRUE\", \"entreprise\":\"FALSE\"}\');"
        cur.execute(sql)
        conn.commit()
    else:
        print(">> Erreur type organisateur")
    
    print("\n>> Insertion championnat: OK")



def insertionEquipe():
    print("\nInsertion d'une equipe:")
    print("-------------------------")
    nom = input("Nom: ")
    ville = input("Ville: ")
    surnoms = ""
    staff = ""

    # Les surnoms --------------------------------------------------------
    nb_surnoms = int(input("Combien de surnoms posséde l'équipe ? "))

    if(nb_surnoms != 0):
        surnoms = "["
        while nb_surnoms != 0:
            surnoms += "{\"surnom\":\""
            surnoms += input("surnom: ")
            if(nb_surnoms != 1):
                surnoms += "\"},"
            elif(nb_surnoms == 1):
                surnoms += "\"}]"
            nb_surnoms -= 1
        print(surnoms)



    # Le staff -----------------------------------------------------------
    nb_staff = int(input("Combien de personne compose le staff ? "))

    if(nb_staff != 0):
        staff = "["
        while nb_staff != 0:
            print ()
            staff += "{\"type\":\""
            staff += input("Type: ")
            staff += "\", \"numSecu\":\""
            staff += input("Numéro Secu: ")
            staff += "\", \"nom\":\""
            staff += input("Nom: ")
            staff += "\", \"prenom\":\""
            staff += input("Prenom: ")
            staff += "\", \"ddn\":\""
            staff += input("Date de naissance (MM/DD/YYYY): ")
            if(nb_staff != 1):
                staff += "\"},"
            elif (nb_staff == 1):
                staff += "\"}]"
            nb_staff -= 1
        print(staff)

        sql = "INSERT INTO Equipe VALUES (\'" + nom + "\', \'" + ville + "\',\'" + surnoms + "\', \'" + staff + "\');"
        cur.execute(sql)
        conn.commit()
        print("\n>> Insertion equipe: OK")


# Menu pour l'insertion de donnees
def routineInsertion():
    var_quit = 0
    while var_quit != 4:
        print("\n\n**************************")
        print("Menu insertion ***********")
        print("**************************")
        print("1- Inserer une equipe")
        print("2- Inserer un championnat")
        print("3- Inserer une ville")
        print("4- Quitter")

        #while var_quit <= 0 and var_quit >= 4:
        var_quit = int(input('Que voulez-vous faire ? '))

        if(var_quit == 1):
            insertionEquipe()
        elif(var_quit == 2):
            insertionChampionnat()
        elif(var_quit == 3):
            insertionVille()
        elif(var_quit == 4):
            print(">> Fin insertion")
        else:
            print("Choix incorrect")




# Les select ******************************************************************************

# Afficher les villes présentes dans la base de données 
def afficherVilles():
    print("\nLes villes présentent dans la base de données sont :\n") 
    sql = "SELECT v.nom, v.pays->>'pays' as pays, v.pays->>'code' as codePays, v.nbStades, v.population FROM Ville v;" 
    cur.execute(sql) 
    raw = cur.fetchone() 
    while raw:     
        print ("Nom: " + raw[0] + "\tPays: " + raw[1]+"\tCodePays: " + raw[2]+"\tNbStades: " + str(raw[3])+"\tPopulation: " + str(raw[4]))     
        raw = cur.fetchone() 

# Afficher les championnats présents dans la base de données
def afficherChampionnats():
    print("\nLes championnats présents dans la base de données sont :\n") 
    sql = "SELECT c.nom, c.dateDebut, c.dateFin, c.organisateur->>'nom',c.organisateur->>'association',c.organisateur->>'entreprise' FROM Championnat c ;" 
    cur.execute(sql) 
    raw = cur.fetchone() 
    while raw:     
        print ("Nom: " + raw[0] + "\tDateDebut: " + str(raw[1])+"\tDateFin: " + str(raw[2])+"\tOrganisateur: " + raw[3]+"\tAssociation: " + str(raw[4])+"\tEntreprise: " + str(raw[5]))     
        raw = cur.fetchone() 

# Afficher les equipes qui participent à un championnat
def afficherParticipant():
    print("\nLes participants présents dans la base de données :\n") 
    sql = "SELECT p.equipe, p.ville, p.championnat, p.dateDebut  FROM Participant p ;" 
    cur.execute(sql) 
    raw = cur.fetchone() 
    while raw:     
        print ("Equipe: " + raw[0] + "\tVille: " + raw[1]+"\tChampionnat: " + raw[2]+"\tDateDebut: " + str(raw[3]))     
        raw = cur.fetchone() 

# Afficher les equipes qui participent a un championnat x
def afficherParticipantChampionnat(championnat):
    print("\nTous les noms des équipes participant au " + championnat + " :\n") 
    championnat ="'" + championnat + "'"
    sql = "SELECT p.championnat, p.equipe FROM Participant p WHERE p.championnat= " + championnat + ";" 
    cur.execute(sql) 
    raw = cur.fetchone() 
    while raw:     
        print ("Championnat: " + raw[0] + " Equipe: " + raw[1])     
        raw = cur.fetchone() 

# On veut afficher les surnoms d'une equipe x
def afficherSurnoms(equipe):
    print("\nLes surnoms de l'equipe \""+ equipe +"\" sont :\n")
    sql = "SELECT e.nom, s->>'surnom' as surnom FROM Equipe e, JSON_ARRAY_ELEMENTS(e.surnoms) s WHERE e.nom = \'" + equipe + "\';"
    cur.execute(sql)
    raw = cur.fetchone()
    while raw:
        print ("\t- " + raw[1])
        raw = cur.fetchone()



# Menu pour la consultation des donnees
def routineConsultation():
    var_quit = 0
    while var_quit != 6:
        print("\n\n*********************************")
        print("Menu consultation BDD ***********")
        print("*********************************")
        print("1- Afficher les villes")
        print("2- Afficher les championnats")
        print("3- Afficher les surnoms de l'equipe x")
        print("4- Afficher les equipes qui participent à un championnat")
        print("5- Afficher les equipes qui participent a un championnat x")
        print("6- Quitter")

        #while var_quit <= 0 and var_quit >= 4:
        var_quit = int(input('Que voulez-vous faire ? '))

        if(var_quit == 1):
            afficherVilles()
        elif(var_quit == 2):
            afficherChampionnats()
        elif(var_quit == 3):
            nom_equipe = input("Nom de l'equipe: ")
            afficherSurnoms(nom_equipe)
        elif(var_quit == 4):
            afficherParticipant()
        elif(var_quit == 5):
            nom_championnat = input("Nom du championnat: ")
            afficherParticipantChampionnat(nom_championnat)
        elif(var_quit == 6):
            print(">> Fin consultation")
        else:
            print("Choix incorrect")







# ----------------------------------------------------------------------------------------------------------
# Menu de l'application ------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------

print ("\n*************************************************************************")
print ("********************** BDD Championnat de football **********************")
print ("*************************************************************************")

var_quit = 0

while var_quit != 3:
    print("\n\n***************************************")
    print("Menu Principal ************************")
    print("***************************************")
    print("1- Inserer des données")
    print("2- Consulter la base de données")
    print("3- Quitter")

    #while var_quit <= 0 and var_quit >= 4:
    var_quit = int(input('Que voulez-vous faire ? '))

    if(var_quit == 1):
        routineInsertion()
    elif(var_quit == 2):
        routineConsultation()
    elif(var_quit == 3):
        print(">> Fin")
    else:
        print("Choix incorrect")









# Close connection
conn.close()
