#### NF17 - TD G3

# Preuve de normalisation

##### Sujet : Championnats de football


Projet réalisé par :

- Clément MOUCHET
- Florent GAUDIN
- Xuefeng HU
- Zilu ZHENG
- Rémi BESOIN

----------------------------------------------------

Ville(#nom: varchar, pays: varchar, nombreDeStades: int, population: int)

    DF: nom -> pays, nombreDeStades, population

    Clé: nom

    1NF:
    Oui, posséde une clé et les attributs sont atomiques.
    
    2NF:
    Oui, la seule clé candidate est formée d'un seul attribut. Donc les attributs qui ne sont pas "nom" ne dépendent pas d'une partie d'une clé.

    3NF:
    Oui, les attributs qui ne sont pas "nom" dépendent directement que de "nom" qui est l'unique clé candidate.

Entraineur(#numSecu: varchar, nom: varchar, prenom: varchar, dateDeNaissance: date)

    DF: numSecu -> nom, prenom, dateDeNaissance

    Clé: numSecu

    1NF:
    Oui, posséde une clé et les attributs sont atomiques.
    
    2NF:
    Oui, la seule clé candidate est formée d'un seul attribut. Donc les attributs qui ne sont pas "numSecu" ne dépendent pas d'une partie d'une clé.

    3NF:
    Oui, les attributs qui ne sont pas "numSecu" dépendent directement que de "numSecu" qui est l'unique clé candidate.


Equipe(#nom: varchar, villeOrigine=>Ville, entraineur=>Entraineur)

    DF: nom -> villeOrigine, entraineur

    Clé: nom

    1NF:
    Oui, posséde une clé et les attributs sont atomiques.
    
    2NF:
    Oui, la seule clé candidate est formée d'un seul attribut. Donc les attributs qui ne sont pas "nom" ne dépendent pas d'une partie d'une clé.

    3NF:
    Oui, les attributs qui ne sont pas "nom" dépendent directement que de "nom" qui est l'unique clé candidate.

SurnomEquipe(#nom: varchar, equipe=>Equipe)

    DF: nom -> equipe

    Clé: nom

    1NF:
    Oui, posséde une clé et les attributs sont atomiques.
    
    2NF:
    Oui, la seule clé candidate est formée d'un seul attribut. Donc l'attribut qui n'est pas "nom" ne dépend pas d'une partie d'une clé.

    3NF:
    Oui, l'attribut qui n'est pas "nom" dépend directement que de "nom" qui est l'unique clé candidate.

Joueur(#numSecu: varchar, nom: varchar, prenom: varchar, dateDeNaissance: date, surnom: varchar, poste: {gardien, defenseur, milieu, attaquant}, experience: int, equipe=>Equipe)

    DF: numSecu -> nom, prenom, dateDeNaissance, surnom, poste, experience, equipe

    Clé: numSecu

    1NF:
    Oui, posséde une clé et les attributs sont atomiques.
    
    2NF:
    Oui, la seule clé candidate est formée d'un seul attribut. Donc les attributs qui ne sont pas "numSecu" ne dépendent pas d'une partie d'une clé.

    3NF:
    Oui, les attributs qui ne sont pas "numSecu" dépendent directement que de "numSecu" qui est l'unique clé candidate.

Arbitre(#numSecu: varchar, nom: varchar, prenom: varchar, dateDeNaissance: date)

    DF: numSecu -> nom, prenom, dateDeNaissance

    Clé: numSecu

    1NF:
    Oui, posséde une clé et les attributs sont atomiques.
    
    2NF:
    Oui, la seule clé candidate est formée d'un seul attribut. Donc les attributs qui ne sont pas "numSecu" ne dépendent pas d'une partie d'une clé.

    3NF:
    Oui, les attributs qui ne sont pas "numSecu" dépendent directement que de "numSecu" qui est l'unique clé candidate.

Championnat(#nom: varchar, #dateDebut: date, dateFin: date, organisateur: varchar)

    DF: (nom,dateDebut) -> dateFin, organisateur

    Clé: (nom,dateDebut)

    1NF:
    Oui, posséde une clé et les attributs sont atomiques.
    
    2NF:
    Oui, Les attributs qui ne font pas partie de l'unique clé candidate (nom,dateDebut), ne dépendent pas d'une partie de cette clé.

    3NF:
    Oui, les attributs qui ne font pas partie de l'unique clé candidate (nom,dateDebut) dépendent directement que de cette clé.


Classement(#nomChampionnat=>Championnat(nom), #dateChampionnat=>Championnat(dateDebut), #equipe=>Equipe)

    DF: (nomChampionnat,dateChampionnat, equipe) -> nomChampionnat,dateChampionnat, equipe

    Clé: (nomChampionnat,dateChampionnat, equipe)

    1NF:
    Oui, posséde une clé et les attributs sont atomiques.
    
    2NF:
    Oui, tout les attributs font partie de l'unique clé candidate (il n'existe pas d'attribut qui ne fait pas partie d'une clé candidate).

    3NF:
    Oui, tout les attributs font partie de l'unique clé candidate (il n'existe pas d'attribut qui ne fait pas partie d'une clé candidate).


Sponsor(#nom: varchar, importance:int)

    DF: nom -> importance

    Clé: nom

    1NF:
    Oui, posséde une clé et les attributs sont atomiques.
    
    2NF:
    Oui, la seule clé candidate est formée d'un seul attribut. Donc l'attribut qui n'est pas "nom" ne dépend pas d'une partie d'une clé.

    3NF:
    Oui, l'attribut qui n'est pas "nom" dépend directement que de "nom" qui est l'unique clé candidate.

Contrat_sponsor(#nomSponsor=>Sponsor, #nomChampionnat=>Championnat(nom), #dateChampionnat=>Championnat(dateDebut))

    DF: (nomSponsor, nomChampionnat,dateChampionnat) -> nomSponsor, nomChampionnat,dateChampionnat

    Clé: (nomSponsor, nomChampionnat,dateChampionnat)

    1NF:
    Oui, posséde une clé et les attributs sont atomiques.
    
    2NF:
    Oui, tout les attributs font partie de l'unique clé candidate (il n'existe pas d'attribut qui ne fait pas partie d'une clé candidate).

    3NF:
    Oui, tout les attributs font partie de l'unique clé candidate (il n'existe pas d'attribut qui ne fait pas partie d'une clé candidate).


Match(#idMatch: int, date: date, arbitreCentral=>Arbitre, arbitreAssistantUn=>Arbitre, arbitreAssistantDeux=>Arbitre, arbitreRemplacant=>Arbitre, ville=>Ville, equipeLocale=>Equipe, equipeVisiteur=>Equipe, nomChampionnat=>Championnat(nom), dateChampionnat=>Championnat(dateDebut))

    DF:
    idMatch -> date, arbitreCentral, arbitreAssistantUn, arbitreAssistantDeux, arbitreRemplacant, ville, equipeLocale, equipeVisiteur, nomChampionnat, dateChampionnat

    date, arbitreCentral, arbitreAssistantUn, arbitreAssistantDeux, arbitreRemplacant -> idMatch, ville, equipeLocale, equipeVisiteur, nomChampionnat, dateChampionnat

    Clés candidates :
    idMatch et (date, arbitreCentral, arbitreAssistantUn, arbitreAssistantDeux, arbitreRemplacant)

    1NF:
    Oui, posséde au moins une clé candidate et les attributs sont atomiques.
    
    2NF:
    Oui, les attributs qui ne sont pas "idMatch" ou qui n'appartiennent pas à (date, arbitreCentral, arbitreAssistantUn, arbitreAssistantDeux, arbitreRemplacant) ne dépendent pas d'une partie d'une clé.

    3NF:
    Oui, les attributs qui ne sont pas "idMatch" ou qui n'appartiennent pas à (date, arbitreCentral, arbitreAssistantUn, arbitreAssistantDeux, arbitreRemplacant) dépendent directement que de "idMatch" ou (date, arbitreCentral, arbitreAssistantUn, arbitreAssistantDeux, arbitreRemplacant) qui sont les uniques clés candidates.

estTitulaire(#idMatch=>Match, #titulaire=>Joueur)

    DF: (idMatch, titulaire) -> idMatch, titulaire

    Clé: (idMatch, titulaire)

    1NF:
    Oui, posséde une clé et les attributs sont atomiques.
    
    2NF:
    Oui, tout les attributs font partie de l'unique clé candidate (il n'existe pas d'attribut qui ne fait pas partie d'une clé candidate).

    3NF:
    Oui, tout les attributs font partie de l'unique clé candidate (il n'existe pas d'attribut qui ne fait pas partie d'une clé candidate).

estRemplacant(#idMatch=>Match, #remplacant=>Joueur)

    DF: (idMatch, remplacant) -> idMatch, remplacant

    Clé: (idMatch, remplacant)

    1NF:
    Oui, posséde une clé et les attributs sont atomiques.
    
    2NF:
    Oui, tout les attributs font partie de l'unique clé candidate (il n'existe pas d'attribut qui ne fait pas partie d'une clé candidate).

    3NF:
    Oui, tout les attributs font partie de l'unique clé candidate (il n'existe pas d'attribut qui ne fait pas partie d'une clé candidate).

But(#minute: int, #idMatch=>Match, type: {penalty, coup franc, corner, classique}, numSecuButeur=>Joueur, numSecuPasseur=>Joueur)

    DF: (minute, idMatch) -> type, numSecuButeur

    Clé: (minute, idMatch)

    1NF:
    Oui, posséde une clé et les attributs sont atomiques.
    
    2NF:
    Oui, Les attributs qui ne font pas partie de l'unique clé candidate (minute, idMatch), ne dépendent pas d'une partie de cette clé.

    3NF:
    Oui, les attributs qui ne font pas partie de l'unique clé candidate (minute, idMatch) dépendent directement que de cette clé.

Sanction(#minute : int, #idMatch=>Match, type: {avertissement, expulsion directe, expulsion indirecte}, numSecuSanctionne=>Joueur)

    DF: (minute, idMatch) -> type, numSecuSanctionne

    Clé: (minute, idMatch)

    1NF:
    Oui, posséde une clé et les attributs sont atomiques.
    
    2NF:
    Oui, Les attributs qui ne font pas partie de l'unique clé candidate (minute, idMatch), ne dépendent pas d'une partie de cette clé.

    3NF:
    Oui, les attributs qui ne font pas partie de l'unique clé candidate (minute, idMatch) dépendent directement que de cette clé.

ChangementDeJoueur(#minute: int, #idMatch=>Match, numSecuJoueurIn=>Joueur, numSecuJoueurOut=>Joueur)

    DF: (minute, idMatch) -> numSecuJoueurIn, numSecuJoueurOut

    Clé: (minute, idMatch)

    1NF:
    Oui, posséde une clé et les attributs sont atomiques.
    
    2NF:
    Oui, Les attributs qui ne font pas partie de l'unique clé candidate (minute, idMatch), ne dépendent pas d'une partie de cette clé.

    3NF:
    Oui, les attributs qui ne font pas partie de l'unique clé candidate (minute, idMatch) dépendent directement que de cette clé.

