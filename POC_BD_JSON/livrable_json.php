<html>

  <head>
    <title>Projet_POC_BD_JSON</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
  
  <body>

    <h1>POC BD JSON (Projet NF17 Championnat de football)</h1>
    <p>Florent GAUDIN - Clément MOUCHET - Rémi BESOIN - Xuefeng HU - Zilu ZHENG</p>
    
    <hr><br>

    <table cellpadding="5">
      <tr>
        <td>
          <table border="1">
            <caption>La liste des équipes avec leurs surnoms</caption>
            <tr><th>Equipe</th><th>Surnom</th></tr>
            <?php
            // Connexion à la base de données
            $vHost = 'tuxa.sme.utc';
            $vPort = '5432';
            $vData = 'dbbdd0p060';
            $vUser = 'bdd0p060';
            $vPass = 'cAW2grC4';
            try{
              $vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vData", $vUser, $vPass);
            }catch(PDOException $e) {
              echo ('Erreur: '.$e->getMessage());
            }
            
            // On veut savoir pour chaque equipe ses surnoms
            $vSql ="SELECT e.nom, s->>'surnom' as surnom FROM Equipe e, JSON_ARRAY_ELEMENTS(e.surnoms) s;";
            $vSt = $vConn->prepare($vSql);
            $vSt->execute();
            while ($vResult = $vSt->fetch(PDO::FETCH_ASSOC)) {
              echo '<tr>';
              echo "<td>$vResult[nom]</td>";
              echo "<td>$vResult[surnom]</td>";
              echo '</tr>';
            }
            ?>
          </table>
        </td>
        <td>
          <table border="1">
            <caption>La liste des organisateurs qui sont des entreprises</caption>
            <tr><th>Organisateur/entreprise</th></tr>
            <?php
            
            // On veut récupérer le nom des organisateurs qui sont des entreprises
            $vSql ="SELECT organisateur->>'nom' as organisateur FROM Championnat c WHERE organisateur->>'entreprise' = 'TRUE';";
            $vSt = $vConn->prepare($vSql);
            $vSt->execute();
            while ($vResult = $vSt->fetch(PDO::FETCH_ASSOC)) {
              echo '<tr>';
              echo "<td>$vResult[organisateur]</td>";
              echo '</tr>';
            }
            ?>
          </table>
        </td>
        <td>
          <table border="1">
            <caption>Les villes présentent dans la base de données</caption>
            <tr><th>nom</th><th>codePays</th><th>pays</th><th>nombreDeStades</th><th>population</th></tr>
            <?php
            
            // On veut afficher les villes présentent dans la base de données
            $vSql ="SELECT * FROM v_ville;";
            $vSt = $vConn->prepare($vSql);
            $vSt->execute();
            while ($vResult = $vSt->fetch(PDO::FETCH_ASSOC)) {
              echo '<tr>';
              echo "<td>$vResult[nom]</td>";
              echo "<td>$vResult[codepays]</td>";
              echo "<td>$vResult[pays]</td>";
              echo "<td>$vResult[nbstades]</td>";
              echo "<td>$vResult[population]</td>";
              echo '</tr>';
            }
            ?>
          </table>
        </td>
      </tr>
    </table>
    
    <br/>
    <?php
      // On veut le nom de l'equipe qui a un médecin avec le numSecu = 374830937492048
      $vSql ="SELECT e.nom as equipe FROM Equipe e, JSON_ARRAY_ELEMENTS(e.staff) s WHERE s->>'numSecu' = '374830937492048';";    
      $vSt = $vConn->prepare($vSql);
      $vSt->execute();    
      $vResult = $vSt->fetch(PDO::FETCH_ASSOC);
      echo "<p>L'équipe qui a le médecin avec le numéro de sécurité social 374830937492048 est : <b>$vResult[equipe]</b></p>";

    ?>
  </body>
  
</html>