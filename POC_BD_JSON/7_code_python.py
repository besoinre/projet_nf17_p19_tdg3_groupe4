#!/usr/bin/python3

import psycopg2

HOST = "tuxa.sme.utc"
USER = "bdd0p060"
PASSWORD = "cAW2grC4"
DATABASE = "dbbdd0p060"

# Open connection
conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))

# Open a cursor to send SQL commands
cur = conn.cursor()

# On veut savoir pour chaque equipe ses surnoms
print("\nLes equipes avec leurs surnoms:\n")
sql = "SELECT e.nom, s->>'surnom' as surnom FROM Equipe e, JSON_ARRAY_ELEMENTS(e.surnoms) s;"
cur.execute(sql)
raw = cur.fetchone()
while raw:
    print ("Equipe: " + raw[0] + " surnom: " + raw[1])
    raw = cur.fetchone()


# On veut le nom de l'equipe qui a un médecin avec le numSecu = 374830937492048

sql = "SELECT e.nom as equipe FROM Equipe e, JSON_ARRAY_ELEMENTS(e.staff) s WHERE s->>'numSecu' = '374830937492048';"
cur.execute(sql)
raw = cur.fetchone()
print("\nL'équipe qui a le médecin avec le numéro de sécurité social 374830937492048 est : " + raw[0] +"\n")

# Close connection
conn.close()