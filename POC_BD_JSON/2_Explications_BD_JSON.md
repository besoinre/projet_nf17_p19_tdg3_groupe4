# Note explicative sur ce qu'on a gagné et perdu d'un modèle relationnel à un modèle Relationel-JSON

Passage vers un modèle Relationel-JSON 

(le 16.06.2019) 

Avec le relationel-JSON nous avons à présent la possibilité de créer des attributs non atomique.
Le format JSON permet donc d'apporter une souplesse au niveau du format des données des attributs de nos tables. 

De ce fait le relationel-JSON ne respecte plus la 1NF, il introduit donc une perte de contrôle sur les atributs qui ne sont pas atomique et au format JSON.

Concernant notre projet "Championnat de football", nous avons sélectionner une partie de notre MCD qui est adaptée à l'utilisation du format JSON, ce MCD fait appel à :
- des compositions : staff compose équipe, 
- des data types : pays et organisaterur,
- un attribut multivalué : surnoms.

