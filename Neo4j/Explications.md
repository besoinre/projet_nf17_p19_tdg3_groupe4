# Note explicative sur ce qu'on a gagné et perdu d'un modèle relationnel à une représentation sous forme de graphe

## Passage d'un modèle relationnel vers un modèle non relationnel : Neo4j

*(le 19.05.2019)* <br><br>

Neo4j est un SGBD NoSQL basé sur les graphes. Les éléments stockés dans les bases de données ne sont plus des tuples mais des noeuds et les associations sont représentées
par des arcs entre ces noeuds. Les noeuds et arcs peuvent posséder des propriétés sous forme de couple attribut-valeur. La structure des noeuds et des arcs n'est pas 
prédéfinies ce qui laisse la possibilité d'avoir des éléments ayant des structures différentes et changeantes. Ici on va modifier notre UML, pour avoir un modèle adapté à 
la représentation en graphe (typiquement des association N:M).

Neo4j permet de représenter aisément les relations entre des éléments de classes différentes. Il permet aussi d'exécuter rapidement les requêtes qui nécessitent de 
traiter les arcs entre les noeuds. 
Dans le cas de notre projet "Championnat de football", cela permet de représenter et de visualiser facilement les liens entre différentes entités majeures : 
- les joueurs,
- les équipes,
- les matchs,
- les championnats,
- les sponsors.

Ainsi, cette représentation permet de retourner des résultats lisibles et explicites. S'il on souhaite connaître tous les joueurs ayant participé à un match, il suffit
de chercher les éléments ayant un lien 'participe' vers le match recherché.

Un inconvénient de Neo4j concerne les composition, il n'est pas possible de représenter des éléments n'existant seulement lorsqu'un autre élément existe. Dans notre projet,
cela concerne les événements d'un match, ces derniers n'existent qu'avec un match. On ne peut pas non plus appliquer de contraintes imposées par le sujet : un nombre minimum
de joueurs par équipes, on ne peut pas forcer la valeur de l'attribut 'poste' d'un joueur à appartenir à un champs de valeurs, etc.

Afin d'adapter au mieux notre projet à Neo4j, nous avons modifié notre diagramme UML pour y ajouter une relation de type n:m supplémentaire : entre équipe et joueurs, ainsi
un joueur peut appartenir à plusieurs équipes (par exemple, un joueur peut appartenir à l'équipe de France et au PSG).