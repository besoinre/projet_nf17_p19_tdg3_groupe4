#### NF17 - TD G3

# Modéle Logique de Données du groupe 4

##### Sujet : Championnats de football

##### Objectif : ​ Réaliser une application qui permet de gérer informatiquement un championnat de football.

Projet réalisé par :

- Clément MOUCHET
- Florent GAUDIN
- Xuefeng HU
- Zilu ZHENG
- Rémi BESOIN

git : https://gitlab.utc.fr/besoinre/projet_nf17_p19_tdg3_groupe

<br>

Ville(#nom: varchar, pays: varchar, nombreDeStades: int, population: int)

    - pays et nombreDeStades NOT NULL

Entraineur(#numSecu: varchar, nom: varchar, prenom: varchar, dateDeNaissance: date)

    - nom et prenom NOT NULL
    - dateDeNaissance NOT NULL

Equipe(#nom: varchar, villeOrigine=>Ville, entraineur=>Entraineur)

    - villeOrigine NOT NULL
    - entraineur NOT NULL

SurnomEquipe(#nom: varchar, equipe=>Equipe)

    - equipe NOT NULL

Joueur(#numSecu: varchar, nom: varchar, prenom: varchar, dateDeNaissance: date, surnom: varchar, poste: {gardien, defenseur, milieu, attaquant}, experience: int, equipe=>Equipe)

    - nom et prenom NOT NULL
    - dateDeNaissance NOT NULL
    - dateActuelle-dateDeNaissance > experience

Arbitre(#numSecu: varchar, nom: varchar, prenom: varchar, dateDeNaissance: date)

    - nom et prenom NOT NULL
    - dateDeNaissance NOT NULL

Championnat(#nom: varchar, #dateDebut: date, dateFin: date, organisateur: varchar)
     
    - dateFin NOT NULL
    - organisateur NOT NULL
    - dateDebut < dateFin

Participant(#nomChampionnat=>Championnat(nom), #dateChampionnat=>Championnat(dateDebut), #equipe=>Equipe)

    - Un championnat doit forcément comporter au moins 2 équipes :
      Projection(Participant,nomChampionnat, dateChampionnat) >= 2.Projection(Championnat, nom, dateDebut) 

Sponsor(#nom: varchar, importance:int)

    - importance NOT NULL et 0 < importance <= 10
    
Contrat_sponsor(#nomSponsor=>Sponsor, #nomChampionnat=>Championnat(nom), #dateChampionnat=>Championnat(dateDebut))

Match(#idMatch: int, date: date, arbitreCentral=>Arbitre, arbitreAssistantUn=>Arbitre, arbitreAssistantDeux=>Arbitre, arbitreRemplacant=>Arbitre, ville=>Ville, equipeLocale=>Equipe, equipeVisiteur=>Equipe, nomChampionnat=>Championnat(nom), dateChampionnat=>Championnat(dateDebut))

    - arbitreCentral IS NOT arbitreAssistantUn
    - arbitreCentral IS NOT arbitreAssistantDeux
    - arbitreCentral IS NOT arbitreRemplacant
    - arbitreAssistantUn IS NOT arbitreAssistantDeux
    - arbitreAssistantUn IS NOT arbitreRemplacant
    - arbitreAssistantDeux IS NOT ArbitreRemplacant
    - equipeLocale IS NOT equipeVisiteur
    - date, arbitreCentral, arbitreAssistantUn, arbitreAssistantDeux, arbitreRemplacant, ville, equipeLocale, equipeVisiteur, nomChampionnat et dateChampionnat NOT NULL
    - PROJECTION(Championnat, nom) = PROJECTION(Match, championnat)
    - Les arbitres ne peuvent pas arbitrer deux matchs qui ont la même date.
    - Les équipes ne peuvent pas participer à deux matchs qui ont la même date.
	
estTitulaire(#idMatch=>Match, #titulaire=>Joueur)

    - Un match doit comporter entre 16 et 22 joueurs titulaires : entre 8 et 11 joueurs dans chaque équipes
    
estRemplacant(#idMatch=>Match, #remplacant=>Joueur)

    - Si un Joueur est remplacant il ne peut pas être titulaire dans un même match

But(#minute: int, #idMatch=>Match, type: {penalty, coup franc, corner, classique}, numSecuButeur=>Joueur, numSecuPasseur=>Joueur)

    - type NOT NULL
    - numSecuButeur NOT NULL
    - 0 <= minute <= 120

Sanction(#minute : int, #idMatch=>Match, type: {avertissement, expulsion directe, expulsion indirecte}, numSecuSanctionne=>Joueur)

    - type NOT NULL
    - numSecuSanctionne NOT NULL
    - 0 <= minute <= 120
      
ChangementDeJoueur(#minute: int, #idMatch=>Match, numSecuJoueurIn=>Joueur, numSecuJoueurOut=>Joueur)

    - numSecuJoueurIn et numSecuJoueurOut NOT NULL
    - numSecuJoueurIn IS NOT numSecuJoueurOut
    - 0 <= minute <= 120
    
