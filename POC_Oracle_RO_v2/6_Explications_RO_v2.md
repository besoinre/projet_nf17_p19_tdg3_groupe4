# Note explicative sur ce qu'on a gagné et perdu d'un modèle relationnel à une représentation relationnelle orientée objet

## Passage vers une représentation orientée objet v2 avec tables objets
*(le 02.06.2019)* <br><br>

Avantage: on n'a plus besoin de faire de jointures.

Inconvénient: il est plus compliqué de faire les INSERT car il faut d'abord récupérer les OID via un SELECT (dans le cas où l'enregistrement à insérer contient une référence objet)

## Passage vers une représentation orientée objet

*(le 26.05.2019)* <br><br>

Un SGBDRO permet d'introduire dans un SGBD relationnel les concepts apportés par l'orienté objet. Ainsi, cela permet de dépasser la contrainte d'atomicité de la 1NF en instaurant 
la possibilité de créer des types utilisateurs. Ces types utilisateurs sont associés aux objets de l'orienté objet. Un SGBDRO représente les compositions à l'aide de ces types
utilisateurs en créant des tables imbriquées. Enfin, les attributs multivalués et composés sont aussi représentés en tant que valeur d'un attribut d'une table à l'aide des 
collections et des types utilisateurs.
Un MCD qui fait appel à des compositions, des attributs composés et des attributs multivalués est adapté à une représentation relationnelle orientée objet.
Nous avons effectué des modifications sur notre MCD initial pour l'adapter : 
- Une classe Staff, possédant un attribut type, est ajoutée, elle a une association composition avec équipe,
- Une association de composition est ajoutée entre arbitre et match, arbitre possède désormais un attribut type,
- Un datatype est créé pour l'organisateur du tournois, désormais, l'organisateur est soit une association soit une entreprise,
- Un datatype est créé pour représenter les pays, ce type est utilisé dans la classe ville.

Étant donné qu'un SGBDRO reprend les points positifs d'un SGBDR et d'un SGBDOO, il permet de créer des bases de données cohérentes tout en instaurant la possibilité de créer des 
tables imbriquées. Les types utilisateurs apportent de nouvelles possibilités : la valeur d'un attribut peut être un objet, une collection d'objet, une collection de valeurs ou
une valeur simple.

Concernant notre projet "Championnat de football" : 
- cela permet de représenter le staff d'une équipe en tant qu'une collection d'objets dans la table équipe,
- chaque match possède une collection de 4 arbitres qui sont des objets,
- Les datatypes sont facilement représentables en orienté objet à l'aide des types utilisateurs, il suffira d'en créer un par datatype.