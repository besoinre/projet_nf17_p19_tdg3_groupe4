# Note de révision

## 14.04.2019

### SQL_LDD_Insert.sql
1) Ajout de données supplémentaires
2) Ajout de "REFRESH MATERIALIZED VIEW ..." après chaque but marqué -> mise à jour automatique du classement

### SQL_LDD_Create.sql
1) La table "Classement" devient "Participant"
2) Ajout des vues matérialisées 

### MLD
1) La relation "Classement" devient "Participant"

### MCD

1) La classe "Classement" devient "Participant"

----------

## 07.04.2019

### MLD
1) Mise à jour du MLD avec les nouvelles clés de Match et Championnat (les relations qui référencent Match ou Championnat ont été mise à jour aussi pour prendre en compte la modification des clés)

### MCD

1) Rectification de l’UML (suppression local key date dans Match, changement de la clé de Championnat)
2) L'attribut minute de la classe Evenement est une clé locale

----------

## 31.03.2019

### MCD

1) Suppression de l'attribut duree dans la classe Match
2) Les associations "est titulaire" et "est remplaçant" sont entre Match et Joueur maintenant
3) Création d'une association (composition) entre Match et Evenement
4) Suppression des associations entre match et les classes filles de Evenement

----------

## 24.03.2019

### MCD

1) Ajout de compositions entre Match et But / Sanction / ChangementDeJoueur.
2) Ajout agrégations.
3) Ajout numSecu dans classe "Personne".
4) Ajout XOR.
5) Modification de titulaire et remplaçant.
6) Modification équipe est composé de 8 joueurs et non de 11 comme dans la réalité.
7) Classement est utilisé pour savoir qui a le plus grand nombre de points (comme précisé en TD)

----------


## 13.03.2019

### NDC

1) Modification de la présentation pour passer sour forme de liste.
2) Le score doit maintenant être mis à jour dès qu'un but est marqué, l'administrateur n'a pas besoin de le faire.
3) Précision des attributs pour les arbitres, matchs.


---------