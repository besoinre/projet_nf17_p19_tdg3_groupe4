# Note explicative sur ce qu'on a gagné et perdu en passant à l'orienté document (JSON)

## Passage d'un modèle relationnel vers un modèle non relationnel : MongoDB

*(le 05.05.2019)* <br><br>

MongoDB est une base de données NoSQL orienté document. Les éléments stockés dans les bases de données ne sont plus des tuples mais des documents sous le format JSON. 
La structure des documents d'une collection n'est pas prédéfinies ni fixe ce qui laisse la possibilité de créer des collections de documents de structures différentes.

MongoDB permet aussi de représenter les relations hiérarchiques aisément. Cela permet dans notre projet "Championnat de football" de représenter un match sous forme de document
qui comportera les détails propres au match ainsi que les événements qui se sont passés lors de ce match. Cette représentation d'un match permet d'accéder à toutes les informations
sans avoir besoin de faire de requêtes pour connaitre l'ensemble des événements qui le composent, nous n'avons plus besoin de faire des jointures. De plus, puisqu'un document n'a pas de structure fixe,
il est possible d'insérer des matchs dans lesquels il n'y a pas eu d'événements et d'autres où il y a eu des événements.
S'il on souhaite, au cours du championnat, ajouter de nouveaux détails aux matchs, c'est faisable sans avoir besoin de modifier les documents déjà existants composant la collection.

Un incovénient de MongoDB concernant notre projet concerne les équipes. Celles-ci sont composées de joueurs, il faudrait par conséquent créer un document équipe dans lequel on 
ajouterait les joueurs. Cependant, il est possible qu'un joueur change d'équipe, cela implique qu'il faut modifier le premier document équipe contenant ce joueur et recrée 
ce même joueur dans le document associé à la nouvelle équipe du joueur. On ne peut également pas vérifier l'unicité des documents.
MongoDB ne permet pas non plus d'appliquer certaines contraintes de notre projet vu qu'il n'y a pas de schéma fixe. Par exemple, on ne peut pas forcer une équipe à posséder
au moins 8 joueurs, on peut pas non plus forcer un champs à appartenir à une liste de valeurs, etc.

Afin d'adapter au mieux notre projet à MongoDB, nous avons modifié notre diagramme UML pour y ajouter des compositions supplémentaires : Championnat-Match, Ville-Equipe, Equipe-Entraineur et Equipe-Joueur.
Ainsi, il est facile de voir l'avantage de MongoDB qui permet de représenter clairement les relations hiérarchiques.