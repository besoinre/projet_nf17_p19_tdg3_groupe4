--
-- SQL INSERT
--
-- Clément Mouchet
-- Florent Gaudin
-- Rémi Besoin
-- Xuefeng Hu
-- Zilu Zheng
--

BEGIN TRANSACTION;
INSERT INTO Ville VALUES ('Paris', 'France', 1, 2141000);
INSERT INTO Ville VALUES ('Lyon', 'France', 1, 998000);
INSERT INTO Ville VALUES ('Marseille', 'France', 1, 1141000);
INSERT INTO Ville VALUES ('Amiens', 'France', 1, 214000);
INSERT INTO Ville VALUES ('Madrid', 'Espagne', 2, 3166000);
INSERT INTO Ville VALUES ('Lisbonne', 'Portugal', 1, 504872);
INSERT INTO Ville VALUES ('Berlin', 'Allemagne', 2, 3575000);
INSERT INTO Ville VALUES ('Hambourg', 'Allemagne', 1, 1787408);
INSERT INTO Ville VALUES ('Munich', 'Allemagne', 3, 1450381);
INSERT INTO Ville VALUES ('Cologne', 'Allemagne', 1, 1060582);
INSERT INTO Ville VALUES ('Stuttgart', 'Allemagne', 1, 623738);
INSERT INTO Ville VALUES ('Rome', 'Italie', 3, 2876019);
INSERT INTO Ville VALUES ('Bruxelles', 'Belgique', 3, 177000);
INSERT INTO Ville VALUES ('Luxembourg', 'Luxembourg', 1, 115000);
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('522-85-0298', 'McTiernan', 'Noëlla', TO_DATE('1/23/1998', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('732-03-5094', 'Olivazzi', 'Marylène', TO_DATE('11/10/1990', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('582-44-7607', 'Anselm', 'Médiamass', TO_DATE('3/8/1989', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('232-03-3709', 'McMackin', 'Maïlys', TO_DATE('11/8/1996', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('438-30-8224', 'Runacres', 'Françoise', TO_DATE('9/16/1981', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('116-82-1232', 'Lambie', 'Stévina', TO_DATE('4/4/1993', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('244-77-2350', 'Kapelhoff', 'Intéressant', TO_DATE('10/17/1982', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('555-83-9019', 'Cracker', 'Dorothée', TO_DATE('6/24/1995', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('577-61-2337', 'Gormley', 'Maïly', TO_DATE('2/25/1999', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('365-46-1173', 'Gynne', 'Maïlis', TO_DATE('3/12/1995', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('686-51-1786', 'Westcarr', 'Annotés', TO_DATE('3/18/1999', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('383-63-6309', 'Sarath', 'Ráo', TO_DATE('10/30/1987', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('518-27-3305', 'Loosemore', 'Méghane', TO_DATE('4/30/1986', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('336-48-6192', 'Lardiner', 'Ophélie', TO_DATE('2/17/1990', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('188-52-7059', 'Krause', 'Wá', TO_DATE('7/26/1994', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('824-89-2271', 'Illsley', 'Görel', TO_DATE('6/13/1991', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('811-26-3130', 'Mulran', 'Athéna', TO_DATE('6/8/1997', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('318-86-0376', 'Nosworthy', 'Léa', TO_DATE('10/15/1995', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('442-80-5357', 'Merwood', 'Mylène', TO_DATE('8/12/1990', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('628-35-6043', 'Loweth', 'Kévina', TO_DATE('1/26/1994', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('224-66-0716', 'MacCaghan', 'Lài', TO_DATE('11/5/1986', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('511-04-6483', 'Maddaford', 'Angélique', TO_DATE('5/14/1986', 'MM/DD/YYYY'));
INSERT INTO Entraineur (numSecu, nom, prenom, dateDeNaissance) VALUES ('123-64-6845', 'Perrit', 'Abbot', TO_DATE('04/15/1976','MM/DD/YYYY'));
COMMIT TRANSACTION;


BEGIN TRANSACTION;
INSERT INTO Equipe VALUES ('France', 'Paris', '522-85-0298');
INSERT INTO Equipe VALUES ('Espagne', 'Madrid', '732-03-5094');
INSERT INTO Equipe VALUES ('Portugal', 'Lisbonne', '582-44-7607');
INSERT INTO Equipe VALUES ('Allemagne', 'Berlin','232-03-3709');
INSERT INTO Equipe VALUES ('Italie', 'Rome','438-30-8224');
INSERT INTO Equipe VALUES ('Belgique', 'Bruxelles','116-82-1232');
INSERT INTO Equipe VALUES ('Luxembourg', 'Luxembourg','123-64-6845');
COMMIT TRANSACTION;


BEGIN TRANSACTION;
INSERT INTO SurnomEquipe VALUES ('Les Bleus', 'France');
INSERT INTO SurnomEquipe VALUES ('La Selección', 'Espagne');
INSERT INTO SurnomEquipe VALUES ('Selecção das quinas', 'Portugal');
INSERT INTO SurnomEquipe VALUES ('Die Mannschaft', 'Allemagne');
INSERT INTO SurnomEquipe VALUES ('Azzurri', 'Italie');
INSERT INTO SurnomEquipe VALUES ('Les Diables rouges', 'Belgique');
INSERT INTO SurnomEquipe VALUES ('Les Lions Rouges', 'Luxembourg');
COMMIT TRANSACTION;


BEGIN TRANSACTION;
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('824-89-2271', 'Illsley', 'Görel', TO_DATE('6/13/1991', 'MM/DD/YYYY'), 'Zoolab', 'attaquant', 3, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('811-26-3130', 'Mulran', 'Athéna', TO_DATE('6/8/1997', 'MM/DD/YYYY'), null, 'defenseur', 6, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('318-86-0376', 'Nosworthy', 'Léa', TO_DATE('10/15/1995', 'MM/DD/YYYY'), 'Stim', 'attaquant', 11, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('442-80-5357', 'Merwood', 'Mylène', TO_DATE('8/12/1990', 'MM/DD/YYYY'), 'Span', 'attaquant', 12, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('628-35-6043', 'Loweth', 'Kévina', TO_DATE('1/26/1994', 'MM/DD/YYYY'), null, 'attaquant', 8, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('224-66-0716', 'MacCaghan', 'Lài', TO_DATE('11/5/1986', 'MM/DD/YYYY'), 'Zoolab', 'gardien', 7, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('511-04-6483', 'Maddaford', 'Angélique', TO_DATE('5/14/1986', 'MM/DD/YYYY'), null, 'milieu', 4, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('293-65-6353', 'Parham', 'Erwéi', TO_DATE('10/26/1991', 'MM/DD/YYYY'), 'Fintone', 'milieu', 9, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('572-23-0440', 'Stedall', 'Josée', TO_DATE('4/2/1989', 'MM/DD/YYYY'), null, 'attaquant', 7, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('196-22-8076', 'D''Ambrogi', 'Naéva', TO_DATE('7/24/1998', 'MM/DD/YYYY'), null, 'attaquant', 5, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('695-14-2076', 'Bartlomieczak', 'Maï', TO_DATE('5/14/1988', 'MM/DD/YYYY'), null, 'attaquant', 9, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('599-34-2166', 'Galland', 'Adèle', TO_DATE('7/8/1996', 'MM/DD/YYYY'), null, 'defenseur', 13, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('355-04-9975', 'Northedge', 'Marie-ève', TO_DATE('9/14/1998', 'MM/DD/YYYY'), 'Solarbreeze', 'gardien', 13, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('647-70-2710', 'Matushevich', 'Gaëlle', TO_DATE('12/29/1993', 'MM/DD/YYYY'), null, 'defenseur', 6, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('568-84-0332', 'Dyer', 'Illustrée', TO_DATE('1/21/1990', 'MM/DD/YYYY'), null, 'defenseur', 10, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('517-99-7315', 'Maken', 'Mahélie', TO_DATE('12/14/1983', 'MM/DD/YYYY'), null, 'attaquant', 10, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('598-69-9395', 'Grigoliis', 'Dà', TO_DATE('1/21/1992', 'MM/DD/YYYY'), null, 'attaquant', 4, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('156-66-7042', 'Pickles', 'Célia', TO_DATE('8/5/1987', 'MM/DD/YYYY'), 'Sonsing', 'gardien', 4, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('843-42-7361', 'Dreini', 'Annotés', TO_DATE('1/17/1981', 'MM/DD/YYYY'), 'Y-find', 'defenseur', 11, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('672-21-6357', 'Hansod', 'Märta', TO_DATE('2/11/1987', 'MM/DD/YYYY'), 'Voltsillam', 'gardien', 12, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('197-76-4764', 'Lidgate', 'Zhì', TO_DATE('4/27/1991', 'MM/DD/YYYY'), 'Tres-Zap', 'milieu', 2, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('481-98-9388', 'Forde', 'Hélène', TO_DATE('10/8/1992', 'MM/DD/YYYY'), 'Stronghold', 'attaquant', 8, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('347-70-4084', 'O''Lagene', 'Méng', TO_DATE('4/8/1999', 'MM/DD/YYYY'), 'Konklux', 'defenseur', 8, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('104-33-0231', 'Bevens', 'Angélique', TO_DATE('8/8/1980', 'MM/DD/YYYY'), null, 'gardien', 3, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('813-14-3836', 'Johann', 'Mélissandre', TO_DATE('12/29/1984', 'MM/DD/YYYY'), 'Stronghold', 'attaquant', 13, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('222-17-6588', 'Graddon', 'Gaëlle', TO_DATE('1/22/1988', 'MM/DD/YYYY'), 'Temp', 'gardien', 6, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('122-35-7825', 'Nicklen', 'Lèi', TO_DATE('8/18/1993', 'MM/DD/YYYY'), null, 'gardien', 4, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('426-57-6943', 'Cail', 'Mahélie', TO_DATE('2/4/1995', 'MM/DD/YYYY'), 'Bitwolf', 'gardien', 8, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('300-02-1467', 'Sickling', 'Loïca', TO_DATE('12/25/1983', 'MM/DD/YYYY'), null, 'attaquant', 12, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('223-06-2131', 'De Paoli', 'Yáo', TO_DATE('5/6/1995', 'MM/DD/YYYY'), null, 'attaquant', 3, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('682-19-9607', 'Neylan', 'Märta', TO_DATE('3/22/1982', 'MM/DD/YYYY'), 'Toughjoyfax', 'gardien', 9, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('596-16-0477', 'Harcarse', 'Vénus', TO_DATE('11/13/1998', 'MM/DD/YYYY'), null, 'gardien', 8, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('479-70-1380', 'Brislen', 'Maëlys', TO_DATE('2/3/1991', 'MM/DD/YYYY'), null, 'defenseur', 4, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('467-35-1071', 'Brahm', 'Estée', TO_DATE('12/5/1987', 'MM/DD/YYYY'), null, 'attaquant', 1, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('239-94-2481', 'O''Donegan', 'Almérinda', TO_DATE('7/14/1986', 'MM/DD/YYYY'), null, 'gardien', 9, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('172-34-4374', 'Balderstone', 'Loïs', TO_DATE('7/21/1997', 'MM/DD/YYYY'), null, 'gardien', 3, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('528-86-5382', 'Pirdy', 'Marie-françoise', TO_DATE('6/22/1992', 'MM/DD/YYYY'), 'Asoka', 'defenseur', 6, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('769-56-4105', 'Gerriet', 'Sòng', TO_DATE('2/2/1992', 'MM/DD/YYYY'), 'Bytecard', 'gardien', 11, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('613-59-2911', 'Sanpher', 'Maëlys', TO_DATE('4/12/1984', 'MM/DD/YYYY'), null, 'defenseur', 1, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('884-95-2695', 'Seres', 'Maëlys', TO_DATE('1/15/1981', 'MM/DD/YYYY'), null, 'attaquant', 2, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('212-76-7447', 'Rickeard', 'Stéphanie', TO_DATE('1/30/1999', 'MM/DD/YYYY'), 'Vagram', 'attaquant', 3, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('350-71-7042', 'Urpeth', 'Göran', TO_DATE('3/14/1999', 'MM/DD/YYYY'), 'Home Ing', 'gardien', 12, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('570-15-8334', 'Popeley', 'Tú', TO_DATE('2/6/1987', 'MM/DD/YYYY'), 'Tres-Zap', 'defenseur', 4, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('801-58-7116', 'Banbrick', 'Lyséa', TO_DATE('12/31/1980', 'MM/DD/YYYY'), null, 'attaquant', 7, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('572-54-4116', 'Crowd', 'Börje', TO_DATE('6/17/1987', 'MM/DD/YYYY'), 'Bitchip', 'milieu', 7, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('387-49-9473', 'Chalice', 'Uò', TO_DATE('6/26/1989', 'MM/DD/YYYY'), null, 'milieu', 5, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('377-32-8903', 'Antonias', 'Léonore', TO_DATE('6/2/1990', 'MM/DD/YYYY'), 'Gembucket', 'attaquant', 2, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('558-58-6000', 'Dwire', 'Noëlla', TO_DATE('3/15/1995', 'MM/DD/YYYY'), 'Fixflex', 'attaquant', 8, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('748-45-0315', 'Memmory', 'Méthode', TO_DATE('7/4/1980', 'MM/DD/YYYY'), 'Daltfresh', 'defenseur', 12, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('306-37-9570', 'Laffranconi', 'Vérane', TO_DATE('1/31/1981', 'MM/DD/YYYY'), null, 'attaquant', 4, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('218-43-2649', 'Valance', 'Léana', TO_DATE('11/22/1997', 'MM/DD/YYYY'), 'Rank', 'defenseur', 9, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('151-95-2586', 'Mogra', 'Maïwenn', TO_DATE('5/29/1988', 'MM/DD/YYYY'), 'Bitwolf', 'attaquant', 8, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('181-45-3926', 'Bootland', 'Geneviève', TO_DATE('9/29/1987', 'MM/DD/YYYY'), null, 'milieu', 6, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('615-28-4750', 'Dranfield', 'Mén', TO_DATE('8/19/1991', 'MM/DD/YYYY'), 'Zamit', 'attaquant', 11, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('634-17-7252', 'Lymer', 'Mégane', TO_DATE('2/28/1990', 'MM/DD/YYYY'), 'Vagram', 'gardien', 1, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('811-19-6435', 'Howard', 'Intéressant', TO_DATE('12/17/1993', 'MM/DD/YYYY'), null, 'milieu', 10, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('434-39-6268', 'Sorsbie', 'Maëlyss', TO_DATE('12/29/1988', 'MM/DD/YYYY'), null, 'attaquant', 4, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('672-25-1926', 'Benthall', 'Cécilia', TO_DATE('4/30/1992', 'MM/DD/YYYY'), null, 'milieu', 13, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('482-29-8334', 'Behnke', 'Angèle', TO_DATE('6/26/1990', 'MM/DD/YYYY'), 'Zoolab', 'attaquant', 11, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('389-95-2189', 'Godin', 'Dà', TO_DATE('4/13/1996', 'MM/DD/YYYY'), null, 'attaquant', 2, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('758-81-4253', 'Nigh', 'Cléopatre', TO_DATE('7/21/1985', 'MM/DD/YYYY'), 'Ronstring', 'defenseur', 4, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('755-66-4432', 'Gibbard', 'Clémence', TO_DATE('3/15/1995', 'MM/DD/YYYY'), 'Zamit', 'gardien', 3, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('830-70-5000', 'Gerritsma', 'Nuó', TO_DATE('4/16/1997', 'MM/DD/YYYY'), 'Span', 'gardien', 11, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('712-47-4551', 'Frend', 'Marylène', TO_DATE('3/15/1985', 'MM/DD/YYYY'), null, 'defenseur', 8, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('734-41-1490', 'Vedeshkin', 'Laurène', TO_DATE('1/9/1989', 'MM/DD/YYYY'), null, 'milieu', 1, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('310-03-5220', 'Gwalter', 'Cinéma', TO_DATE('7/29/1981', 'MM/DD/YYYY'), null, 'gardien', 5, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('711-46-4449', 'Chopping', 'Marie-josée', TO_DATE('7/25/1984', 'MM/DD/YYYY'), null, 'defenseur', 6, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('483-70-3514', 'Michelle', 'Cunégonde', TO_DATE('12/29/1985', 'MM/DD/YYYY'), 'Y-find', 'attaquant', 10, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('296-53-5721', 'Lesper', 'Josée', TO_DATE('8/4/1992', 'MM/DD/YYYY'), null, 'gardien', 12, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('852-68-6050', 'de Werk', 'Méthode', TO_DATE('11/9/1980', 'MM/DD/YYYY'), 'Sub-Ex', 'defenseur', 8, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('116-85-1811', 'Laraway', 'Aimée', TO_DATE('7/12/1993', 'MM/DD/YYYY'), 'Gembucket', 'attaquant', 11, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('220-64-9061', 'Le Strange', 'Véronique', TO_DATE('8/12/1997', 'MM/DD/YYYY'), 'Pannier', 'defenseur', 11, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('227-93-8851', 'Baldam', 'Méline', TO_DATE('1/12/1998', 'MM/DD/YYYY'), 'Stim', 'attaquant', 6, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('818-63-4878', 'Follis', 'Méline', TO_DATE('7/14/1989', 'MM/DD/YYYY'), 'Flexidy', 'gardien', 8, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('121-45-4593', 'Dongate', 'Mélina', TO_DATE('10/10/1997', 'MM/DD/YYYY'), null, 'milieu', 2, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('507-18-5408', 'Mapledorum', 'Bénédicte', TO_DATE('10/12/1983', 'MM/DD/YYYY'), null, 'gardien', 7, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('375-68-3569', 'Murrish', 'Vérane', TO_DATE('1/22/1992', 'MM/DD/YYYY'), null, 'attaquant', 5, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('774-27-1632', 'Saturley', 'Eloïse', TO_DATE('11/12/1995', 'MM/DD/YYYY'), 'Kanlam', 'defenseur', 10, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('163-02-9910', 'Chill', 'Tán', TO_DATE('7/29/1983', 'MM/DD/YYYY'), 'Duobam', 'gardien', 4, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('771-30-3457', 'Vlahos', 'Maïlys', TO_DATE('11/30/1990', 'MM/DD/YYYY'), 'Sub-Ex', 'gardien', 7, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('413-44-4776', 'MacDunlevy', 'Renée', TO_DATE('12/23/1982', 'MM/DD/YYYY'), 'Tempsoft', 'defenseur', 2, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('554-84-9816', 'Gravell', 'Publicité', TO_DATE('7/29/1988', 'MM/DD/YYYY'), 'Biodex', 'milieu', 4, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('450-86-7182', 'Enrique', 'Maïlis', TO_DATE('3/26/1992', 'MM/DD/YYYY'), null, 'defenseur', 9, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('263-29-5955', 'Rake', 'Eliès', TO_DATE('11/10/1993', 'MM/DD/YYYY'), null, 'milieu', 7, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('461-74-8221', 'Martschik', 'Valérie', TO_DATE('4/8/1989', 'MM/DD/YYYY'), 'Lotlux', 'milieu', 3, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('676-46-6361', 'Wickendon', 'Maïté', TO_DATE('8/5/1993', 'MM/DD/YYYY'), 'Aerified', 'defenseur', 5, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('414-62-6344', 'Vanyukhin', 'Mélissandre', TO_DATE('2/16/1986', 'MM/DD/YYYY'), null, 'gardien', 10, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('245-14-3305', 'Maccaddie', 'Táng', TO_DATE('4/18/1993', 'MM/DD/YYYY'), 'Kanlam', 'milieu', 9, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('766-95-6236', 'Dinning', 'Renée', TO_DATE('5/2/1998', 'MM/DD/YYYY'), 'Sonsing', 'gardien', 7, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('145-39-9473', 'Geare', 'Agnès', TO_DATE('3/17/1996', 'MM/DD/YYYY'), 'Vagram', 'milieu', 9, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('763-31-2599', 'Yoakley', 'Kù', TO_DATE('2/16/1981', 'MM/DD/YYYY'), null, 'defenseur', 11, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('491-59-4635', 'Casetta', 'Tán', TO_DATE('1/9/1999', 'MM/DD/YYYY'), null, 'gardien', 7, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('294-82-2609', 'Nash', 'Méryl', TO_DATE('8/22/1996', 'MM/DD/YYYY'), 'It', 'attaquant', 4, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('886-81-1980', 'Lazar', 'Nélie', TO_DATE('10/27/1987', 'MM/DD/YYYY'), null, 'gardien', 5, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('669-33-9843', 'Yeulet', 'Måns', TO_DATE('9/19/1981', 'MM/DD/YYYY'), null, 'milieu', 10, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('728-35-6240', 'Blazy', 'Mylène', TO_DATE('11/24/1991', 'MM/DD/YYYY'), 'Quo Lux', 'gardien', 1, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('619-86-3630', 'Hillock', 'Kuí', TO_DATE('7/31/1980', 'MM/DD/YYYY'), null, 'defenseur', 12, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('448-34-3646', 'Langman', 'Clélia', TO_DATE('9/30/1988', 'MM/DD/YYYY'), 'Temp', 'gardien', 13, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('758-13-1183', 'Philot', 'Méghane', TO_DATE('12/22/1986', 'MM/DD/YYYY'), 'Home Ing', 'milieu', 8, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('334-03-3194', 'Delamar', 'André', TO_DATE('11/26/1988', 'MM/DD/YYYY'), null, 'gardien', 13, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('709-85-9048', 'Rait', 'Göran', TO_DATE('6/3/1990', 'MM/DD/YYYY'), 'Overhold', 'attaquant', 8, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('285-24-1185', 'D''Costa', 'Marlène', TO_DATE('7/10/1982', 'MM/DD/YYYY'), 'Bitwolf', 'milieu', 6, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('206-59-9903', 'Iglesia', 'Mélodie', TO_DATE('5/26/1995', 'MM/DD/YYYY'), 'Treeflex', 'milieu', 9, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('241-47-8172', 'Blackler', 'Cécile', TO_DATE('9/21/1987', 'MM/DD/YYYY'), 'Bitwolf', 'attaquant', 5, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('762-75-1080', 'Hove', 'Dorothée', TO_DATE('11/16/1986', 'MM/DD/YYYY'), 'Zoolab', 'attaquant', 12, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('859-99-6033', 'Skelhorn', 'Anaël', TO_DATE('10/13/1994', 'MM/DD/YYYY'), 'Rank', 'gardien', 3, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('500-82-6511', 'Machel', 'Solène', TO_DATE('5/22/1981', 'MM/DD/YYYY'), 'Fintone', 'gardien', 1, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('648-96-3088', 'Peteri', 'Lén', TO_DATE('9/4/1980', 'MM/DD/YYYY'), null, 'milieu', 1, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('846-26-4608', 'Macura', 'Lucrèce', TO_DATE('3/20/1982', 'MM/DD/YYYY'), 'Stringtough', 'defenseur', 12, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('491-88-3651', 'Gulc', 'Pénélope', TO_DATE('8/23/1992', 'MM/DD/YYYY'), null, 'milieu', 5, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('382-48-3096', 'Avrahm', 'Åsa', TO_DATE('2/1/1987', 'MM/DD/YYYY'), 'Home Ing', 'attaquant', 2, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('840-06-6601', 'Hedgeley', 'Dorothée', TO_DATE('12/19/1993', 'MM/DD/YYYY'), 'Tresom', 'defenseur', 1, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('485-75-2425', 'Dobbin', 'Mén', TO_DATE('2/25/1987', 'MM/DD/YYYY'), 'Alphazap', 'milieu', 10, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('660-51-1283', 'O''Kerin', 'Andréa', TO_DATE('11/6/1993', 'MM/DD/YYYY'), 'Domainer', 'milieu', 8, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('525-11-2128', 'Dohms', 'Örjan', TO_DATE('9/16/1985', 'MM/DD/YYYY'), null, 'milieu', 6, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('194-69-6814', 'Brasted', 'Maïlis', TO_DATE('10/13/1995', 'MM/DD/YYYY'), null, 'attaquant', 6, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('217-41-2275', 'Trusler', 'Stévina', TO_DATE('1/29/1995', 'MM/DD/YYYY'), null, 'gardien', 13, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('346-77-5649', 'Wooffitt', 'Uò', TO_DATE('9/5/1998', 'MM/DD/YYYY'), null, 'attaquant', 8, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('539-04-1869', 'Langsdon', 'Eloïse', TO_DATE('9/1/1984', 'MM/DD/YYYY'), null, 'milieu', 8, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('628-47-7351', 'McGeneay', 'André', TO_DATE('7/3/1988', 'MM/DD/YYYY'), 'Cardify', 'gardien', 11, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('759-56-1123', 'Fergusson', 'Lóng', TO_DATE('11/17/1989', 'MM/DD/YYYY'), null, 'defenseur', 11, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('786-57-9672', 'Folling', 'Anaëlle', TO_DATE('4/23/1983', 'MM/DD/YYYY'), 'Fix San', 'milieu', 7, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('707-77-0900', 'Folley', 'Marie-ève', TO_DATE('9/5/1983', 'MM/DD/YYYY'), null, 'attaquant', 9, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('395-97-4255', 'Stemson', 'Simplifiés', TO_DATE('8/21/1980', 'MM/DD/YYYY'), null, 'gardien', 13, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('547-06-3055', 'Littrik', 'Håkan', TO_DATE('10/12/1993', 'MM/DD/YYYY'), null, 'milieu', 11, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('293-29-5535', 'Jerger', 'Yénora', TO_DATE('12/19/1995', 'MM/DD/YYYY'), 'Home Ing', 'defenseur', 6, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('188-39-5523', 'Lymer', 'Cléopatre', TO_DATE('11/29/1982', 'MM/DD/YYYY'), 'Matsoft', 'gardien', 11, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('652-27-1659', 'Smail', 'Irène', TO_DATE('4/15/1991', 'MM/DD/YYYY'), 'Treeflex', 'gardien', 6, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('824-24-4988', 'Heifer', 'Léone', TO_DATE('4/28/1997', 'MM/DD/YYYY'), 'Andalax', 'defenseur', 7, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('257-30-7046', 'Waymont', 'Léonore', TO_DATE('4/18/1990', 'MM/DD/YYYY'), 'Matsoft', 'attaquant', 13, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('536-60-9567', 'Leedes', 'Laurène', TO_DATE('9/14/1985', 'MM/DD/YYYY'), null, 'gardien', 7, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('468-07-7504', 'Dunlea', 'Nuó', TO_DATE('5/18/1985', 'MM/DD/YYYY'), 'Wrapsafe', 'attaquant', 7, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('103-96-8690', 'Foakes', 'Estève', TO_DATE('1/14/1984', 'MM/DD/YYYY'), null, 'gardien', 12, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('838-41-7718', 'Elmer', 'Maïlis', TO_DATE('11/28/1994', 'MM/DD/YYYY'), 'Regrant', 'milieu', 2, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('693-45-3640', 'Brearley', 'Torbjörn', TO_DATE('1/28/1997', 'MM/DD/YYYY'), 'Kanlam', 'defenseur', 1, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('328-51-1923', 'McMahon', 'Maïlis', TO_DATE('11/30/1992', 'MM/DD/YYYY'), 'Andalax', 'attaquant', 11, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('748-49-4731', 'Hackly', 'Rébecca', TO_DATE('11/26/1981', 'MM/DD/YYYY'), 'Rank', 'gardien', 6, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('109-26-1686', 'Threlfall', 'Maëlla', TO_DATE('5/24/1984', 'MM/DD/YYYY'), 'Fix San', 'milieu', 4, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('831-27-7485', 'Dziwisz', 'Eléonore', TO_DATE('10/8/1982', 'MM/DD/YYYY'), 'Fintone', 'gardien', 8, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('102-86-2796', 'Scobbie', 'Lucrèce', TO_DATE('3/30/1996', 'MM/DD/YYYY'), 'Flowdesk', 'defenseur', 13, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('850-81-8762', 'McClay', 'Clémentine', TO_DATE('7/26/1998', 'MM/DD/YYYY'), 'Zoolab', 'milieu', 9, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('253-60-3676', 'Clayill', 'Loïca', TO_DATE('12/27/1986', 'MM/DD/YYYY'), null, 'attaquant', 10, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('609-57-1003', 'Reihm', 'Lài', TO_DATE('10/2/1991', 'MM/DD/YYYY'), null, 'attaquant', 5, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('488-35-6679', 'Jansey', 'Gaëlle', TO_DATE('4/10/1998', 'MM/DD/YYYY'), 'Job', 'milieu', 13, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('109-92-5951', 'Yetman', 'Mà', TO_DATE('7/15/1984', 'MM/DD/YYYY'), 'Bamity', 'milieu', 8, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('557-13-3095', 'Hammett', 'Aurélie', TO_DATE('11/28/1995', 'MM/DD/YYYY'), 'Sub-Ex', 'attaquant', 12, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('860-08-4817', 'Kenafaque', 'Josée', TO_DATE('3/27/1999', 'MM/DD/YYYY'), null, 'gardien', 6, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('598-57-3552', 'Zapata', 'Erwéi', TO_DATE('6/19/1983', 'MM/DD/YYYY'), 'Rank', 'defenseur', 1, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('356-50-9967', 'Pohling', 'Méline', TO_DATE('1/5/1992', 'MM/DD/YYYY'), 'Tin', 'attaquant', 11, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('186-54-8110', 'Fruin', 'Maëline', TO_DATE('9/27/1995', 'MM/DD/YYYY'), 'Asoka', 'gardien', 9, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('778-68-3847', 'Gordge', 'Vérane', TO_DATE('10/27/1986', 'MM/DD/YYYY'), null, 'defenseur', 4, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('393-84-0460', 'Scripps', 'Bérangère', TO_DATE('6/23/1994', 'MM/DD/YYYY'), 'Cookley', 'attaquant', 7, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('479-74-4091', 'Siverns', 'Anaé', TO_DATE('4/22/1997', 'MM/DD/YYYY'), 'Ventosanzap', 'defenseur', 1, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('829-82-5705', 'Kerrane', 'Gérald', TO_DATE('1/29/1989', 'MM/DD/YYYY'), 'Veribet', 'milieu', 9, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('709-85-7809', 'O''Hingerty', 'Marie-ève', TO_DATE('2/14/1987', 'MM/DD/YYYY'), null, 'milieu', 5, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('815-86-6942', 'Backshell', 'Angèle', TO_DATE('9/15/1995', 'MM/DD/YYYY'), null, 'attaquant', 2, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('769-46-8158', 'Spellacey', 'Joséphine', TO_DATE('12/11/1993', 'MM/DD/YYYY'), 'Prodder', 'gardien', 2, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('801-57-9657', 'Humberstone', 'Marlène', TO_DATE('5/29/1981', 'MM/DD/YYYY'), null, 'attaquant', 9, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('392-53-4416', 'Prichet', 'Léonie', TO_DATE('3/10/1983', 'MM/DD/YYYY'), null, 'defenseur', 6, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('209-15-5490', 'Pedycan', 'Östen', TO_DATE('5/21/1985', 'MM/DD/YYYY'), null, 'defenseur', 3, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('256-82-6687', 'Attrey', 'Bérangère', TO_DATE('10/6/1984', 'MM/DD/YYYY'), null, 'milieu', 3, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('454-94-7959', 'O''Downe', 'Gérald', TO_DATE('7/2/1984', 'MM/DD/YYYY'), null, 'gardien', 6, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('871-89-6301', 'Houseley', 'Josée', TO_DATE('5/26/1998', 'MM/DD/YYYY'), 'Zontrax', 'gardien', 3, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('399-51-9506', 'Melloi', 'Dorothée', TO_DATE('9/25/1992', 'MM/DD/YYYY'), null, 'defenseur', 7, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('102-96-8783', 'Rawlingson', 'Torbjörn', TO_DATE('1/28/1999', 'MM/DD/YYYY'), null, 'defenseur', 6, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('635-09-3730', 'Roelvink', 'Mélia', TO_DATE('12/31/1988', 'MM/DD/YYYY'), 'Tresom', 'milieu', 1, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('208-36-6000', 'Layne', 'Bérangère', TO_DATE('7/12/1992', 'MM/DD/YYYY'), 'Cardify', 'milieu', 6, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('774-83-8035', 'Bloschke', 'Desirée', TO_DATE('11/15/1985', 'MM/DD/YYYY'), 'Kanlam', 'gardien', 4, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('309-33-3100', 'Ennion', 'Maïlis', TO_DATE('12/15/1994', 'MM/DD/YYYY'), null, 'attaquant', 4, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('537-75-2846', 'Rangall', 'Mélissandre', TO_DATE('9/29/1991', 'MM/DD/YYYY'), null, 'milieu', 4, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('672-83-2949', 'Klauer', 'Maéna', TO_DATE('11/15/1982', 'MM/DD/YYYY'), 'Home Ing', 'attaquant', 12, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('225-34-7067', 'Capstaff', 'Clémentine', TO_DATE('7/22/1988', 'MM/DD/YYYY'), 'Cardify', 'defenseur', 11, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('399-19-8338', 'Malinowski', 'Gaïa', TO_DATE('12/18/1983', 'MM/DD/YYYY'), null, 'milieu', 3, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('467-96-4372', 'Sheldrake', 'Personnalisée', TO_DATE('6/3/1998', 'MM/DD/YYYY'), 'Overhold', 'attaquant', 12, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('295-32-9852', 'Shimmin', 'Anaé', TO_DATE('10/25/1982', 'MM/DD/YYYY'), null, 'milieu', 4, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('726-47-5171', 'Preedy', 'Eugénie', TO_DATE('3/25/1989', 'MM/DD/YYYY'), null, 'attaquant', 8, 'Italie');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('612-35-2938', 'Gilson', 'Pål', TO_DATE('10/7/1996', 'MM/DD/YYYY'), null, 'attaquant', 1, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('783-54-2861', 'Lippitt', 'Desirée', TO_DATE('1/13/1982', 'MM/DD/YYYY'), null, 'gardien', 2, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('338-24-3496', 'Tompkiss', 'Réservés', TO_DATE('4/5/1981', 'MM/DD/YYYY'), null, 'gardien', 10, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('639-11-9444', 'Tynan', 'Pénélope', TO_DATE('8/30/1992', 'MM/DD/YYYY'), 'Bitwolf', 'milieu', 2, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('499-49-4231', 'Bodechon', 'Cléa', TO_DATE('4/10/1992', 'MM/DD/YYYY'), null, 'milieu', 1, 'Portugal');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('343-37-2913', 'Shrigley', 'Béatrice', TO_DATE('1/11/1987', 'MM/DD/YYYY'), 'Aerified', 'defenseur', 13, 'Allemagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('150-37-4420', 'Gabey', 'Desirée', TO_DATE('2/2/1992', 'MM/DD/YYYY'), 'Sub-Ex', 'gardien', 9, 'Espagne');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('485-90-9214', 'Josskowitz', 'Eliès', TO_DATE('8/6/1996', 'MM/DD/YYYY'), null, 'gardien', 6, 'Belgique');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('713-80-8045', 'Gavaran', 'Maëly', TO_DATE('12/28/1997', 'MM/DD/YYYY'), null, 'milieu', 10, 'France');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('865-57-1952', 'Schimtt', 'Aurelien', TO_DATE('11/13/1990','MM/DD/YYYY'), 'Cocina', 'gardien', 15, 'Luxembourg');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('456-77-1234', 'Likolick', 'Herve', TO_DATE('12/03/1997','MM/DD/YYYY'), null, 'gardien', 13, 'Luxembourg');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('429-34-2502', 'Reding', 'Mary', TO_DATE('11/25/1990','MM/DD/YYYY'), null, 'defenseur', 12, 'Luxembourg');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('486-74-1963', 'Genkins', 'Bicor', TO_DATE('09/28/1975','MM/DD/YYYY'), null, 'defenseur', 20, 'Luxembourg');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('197-96-6845', 'Tillie', 'Leeds', TO_DATE('06/23/1994','MM/DD/YYYY'), null, 'defenseur', 15, 'Luxembourg');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('934-55-6548', 'Lee', 'Roy', TO_DATE('05/31/1989','MM/DD/YYYY'), 'book', 'defenseur', 20, 'Luxembourg');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('435-39-6458', 'Hinds', 'Aldo', TO_DATE('10/21/1990','MM/DD/YYYY'), null, 'defenseur', 10, 'Luxembourg');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('456-96-7894', 'Whaley', 'Kenneth', TO_DATE('11/30/1982','MM/DD/YYYY'), 'Kent', 'milieu', 27, 'Luxembourg');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('423-12-7542', 'Bucy', 'John', TO_DATE('01/25/1980','MM/DD/YYYY'), null, 'milieu', 15, 'Luxembourg');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('147-85-7456', 'Lee', 'Roy', TO_DATE('05/31/1989','MM/DD/YYYY'), null, 'milieu', 20, 'Luxembourg');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('496-12-6482', 'Hinds', 'Aldo', TO_DATE('06/10/1990','MM/DD/YYYY'), null, 'milieu', 10, 'Luxembourg');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('863-96-7638', 'Hinds', 'Aldo', TO_DATE('04/10/2001','MM/DD/YYYY'), null, 'milieu', 5, 'Luxembourg');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('159-75-3456', 'Arndt', 'Kenneth', TO_DATE('05/31/1989','MM/DD/YYYY'), null, 'attaquant', 20, 'Luxembourg');
INSERT INTO Joueur (numSecu, nom, prenom, dateDeNaissance, surnom, poste, experience, equipe) VALUES ('438-86-7894', 'Simental', 'Owen', TO_DATE('06/29/1992','MM/DD/YYYY'), null, 'attaquant', 10, 'Luxembourg');
COMMIT TRANSACTION;



BEGIN TRANSACTION;
INSERT INTO Arbitre (numSecu, nom, prenom, dateDeNaissance) VALUES ('183-45-6549', 'Dowall', 'Ardene', TO_DATE('11/2/1995', 'MM/DD/YYYY'));
INSERT INTO Arbitre (numSecu, nom, prenom, dateDeNaissance) VALUES ('587-83-0024', 'O'' Molan', 'Ferd', TO_DATE('7/6/1981', 'MM/DD/YYYY'));
INSERT INTO Arbitre (numSecu, nom, prenom, dateDeNaissance) VALUES ('168-55-4560', 'Witherington', 'Doti', TO_DATE('11/13/1996', 'MM/DD/YYYY'));
INSERT INTO Arbitre (numSecu, nom, prenom, dateDeNaissance) VALUES ('777-92-0130', 'Marishenko', 'Helsa', TO_DATE('10/16/1987', 'MM/DD/YYYY'));
INSERT INTO Arbitre (numSecu, nom, prenom, dateDeNaissance) VALUES ('623-80-5525', 'Franscioni', 'Yorke', TO_DATE('3/18/1981', 'MM/DD/YYYY'));
INSERT INTO Arbitre (numSecu, nom, prenom, dateDeNaissance) VALUES ('894-47-8550', 'Barnaclough', 'Arielle', TO_DATE('12/5/1994', 'MM/DD/YYYY'));
INSERT INTO Arbitre (numSecu, nom, prenom, dateDeNaissance) VALUES ('731-55-0420', 'Jonke', 'Alexio', TO_DATE('3/3/1990', 'MM/DD/YYYY'));
INSERT INTO Arbitre (numSecu, nom, prenom, dateDeNaissance) VALUES ('267-36-2014', 'De Bernardi', 'Hanni', TO_DATE('11/20/1995', 'MM/DD/YYYY'));
INSERT INTO Arbitre (numSecu, nom, prenom, dateDeNaissance) VALUES ('540-08-1530', 'Hattigan', 'Gilberta', TO_DATE('11/21/1984', 'MM/DD/YYYY'));
INSERT INTO Arbitre (numSecu, nom, prenom, dateDeNaissance) VALUES ('218-60-1618', 'LAbbet', 'Sergeant', TO_DATE('7/15/1993', 'MM/DD/YYYY'));
INSERT INTO Arbitre (numSecu, nom, prenom, dateDeNaissance) VALUES ('628-77-4428', 'Hartop', 'Ema', TO_DATE('5/19/1985', 'MM/DD/YYYY'));
INSERT INTO Arbitre (numSecu, nom, prenom, dateDeNaissance) VALUES ('828-42-3900', 'Bradbeer', 'Emmalee', TO_DATE('9/18/1992', 'MM/DD/YYYY'));
INSERT INTO Arbitre (numSecu, nom, prenom, dateDeNaissance) VALUES ('358-45-0143', 'Potteridge', 'Matthew', TO_DATE('3/12/1991', 'MM/DD/YYYY'));
INSERT INTO Arbitre (numSecu, nom, prenom, dateDeNaissance) VALUES ('846-18-4682', 'Di Giacomo', 'Abner', TO_DATE('11/10/1990', 'MM/DD/YYYY'));
INSERT INTO Arbitre (numSecu, nom, prenom, dateDeNaissance) VALUES ('862-57-0108', 'Elphick', 'Nolie', TO_DATE('9/30/1992', 'MM/DD/YYYY'));
INSERT INTO Arbitre (numSecu, nom, prenom, dateDeNaissance) VALUES ('199-37-4485', 'Nike', 'Gaine', TO_DATE('6/29/1989', 'MM/DD/YYYY'));
COMMIT TRANSACTION;


BEGIN TRANSACTION;
INSERT INTO Championnat VALUES('FIFA World Cup', TO_DATE('06/16/2016', 'MM/DD/YYYY'), TO_DATE('07/17/2016', 'MM/DD/YYYY'), 'FIFA');
INSERT INTO Championnat VALUES('FIFA World Cup', TO_DATE('05/13/2012', 'MM/DD/YYYY'), TO_DATE('06/14/2012', 'MM/DD/YYYY'), 'FIFA');
COMMIT TRANSACTION;


BEGIN TRANSACTION;
INSERT INTO Participant VALUES('FIFA World Cup', TO_DATE('06/16/2016', 'MM/DD/YYYY'), 'France');
INSERT INTO Participant VALUES('FIFA World Cup', TO_DATE('06/16/2016', 'MM/DD/YYYY'), 'Espagne');
INSERT INTO Participant VALUES('FIFA World Cup', TO_DATE('06/16/2016', 'MM/DD/YYYY'), 'Portugal');
INSERT INTO Participant VALUES('FIFA World Cup', TO_DATE('06/16/2016', 'MM/DD/YYYY'), 'Allemagne');
INSERT INTO Participant VALUES('FIFA World Cup', TO_DATE('06/16/2016', 'MM/DD/YYYY'), 'Italie');
INSERT INTO Participant VALUES('FIFA World Cup', TO_DATE('06/16/2016', 'MM/DD/YYYY'), 'Belgique');
COMMIT TRANSACTION;
BEGIN TRANSACTION;
INSERT INTO Participant VALUES('FIFA World Cup', TO_DATE('05/13/2012', 'MM/DD/YYYY'), 'France');
INSERT INTO Participant VALUES('FIFA World Cup', TO_DATE('05/13/2012', 'MM/DD/YYYY'), 'Espagne');
INSERT INTO Participant VALUES('FIFA World Cup', TO_DATE('05/13/2012', 'MM/DD/YYYY'), 'Portugal');
INSERT INTO Participant VALUES('FIFA World Cup', TO_DATE('05/13/2012', 'MM/DD/YYYY'), 'Allemagne');
INSERT INTO Participant VALUES('FIFA World Cup', TO_DATE('05/13/2012', 'MM/DD/YYYY'), 'Italie');
INSERT INTO Participant VALUES('FIFA World Cup', TO_DATE('05/13/2012', 'MM/DD/YYYY'), 'Belgique');
COMMIT TRANSACTION;



BEGIN TRANSACTION;
INSERT INTO Sponsor VALUES('Nike', 9);
INSERT INTO Sponsor VALUES('Adidas', 8);
INSERT INTO Sponsor VALUES('Redbull', 3);
INSERT INTO Sponsor VALUES('Kia Motors', 7);
COMMIT TRANSACTION;



BEGIN TRANSACTION;
INSERT INTO Contrat_sponsor VALUES('Adidas', 'FIFA World Cup', TO_DATE('06/16/2016', 'MM/DD/YYYY'));
INSERT INTO Contrat_sponsor VALUES('Kia Motors', 'FIFA World Cup', TO_DATE('06/16/2016', 'MM/DD/YYYY'));
INSERT INTO Contrat_sponsor VALUES('Nike', 'FIFA World Cup', TO_DATE('05/13/2012', 'MM/DD/YYYY'));
INSERT INTO Contrat_sponsor VALUES('Redbull', 'FIFA World Cup', TO_DATE('05/13/2012', 'MM/DD/YYYY'));
COMMIT TRANSACTION;


-- FIFA WORLD CUP 2016
BEGIN TRANSACTION;
INSERT INTO Match VALUES(116, TO_DATE('06/16/2016', 'MM/DD/YYYY'), '183-45-6549', '587-83-0024', '168-55-4560', '777-92-0130', 'Paris', 'France', 'Italie', 'FIFA World Cup', TO_DATE('06/16/2016', 'MM/DD/YYYY'));
INSERT INTO Match VALUES(216, TO_DATE('06/17/2016', 'MM/DD/YYYY'), '623-80-5525', '894-47-8550', '731-55-0420', '267-36-2014', 'Marseille', 'Allemagne', 'Espagne', 'FIFA World Cup', TO_DATE('06/16/2016', 'MM/DD/YYYY'));
INSERT INTO Match VALUES(316, TO_DATE('06/19/2016', 'MM/DD/YYYY'), '540-08-1530', '218-60-1618', '628-77-4428', '828-42-3900', 'Lyon', 'Allemagne', 'France', 'FIFA World Cup', TO_DATE('06/16/2016', 'MM/DD/YYYY'));
INSERT INTO Match VALUES(416, TO_DATE('06/20/2016', 'MM/DD/YYYY'), '358-45-0143', '846-18-4682', '862-57-0108', '199-37-4485', 'Amiens', 'Espagne', 'Italie', 'FIFA World Cup', TO_DATE('06/16/2016', 'MM/DD/YYYY'));
INSERT INTO Match VALUES(516, TO_DATE('06/22/2016', 'MM/DD/YYYY'), '267-36-2014', '731-55-0420', '894-47-8550', '623-80-5525', 'Paris', 'Italie', 'Allemagne', 'FIFA World Cup', TO_DATE('06/16/2016', 'MM/DD/YYYY'));
INSERT INTO Match VALUES(616, TO_DATE('06/23/2016', 'MM/DD/YYYY'), '777-92-0130', '168-55-4560', '587-83-0024', '183-45-6549', 'Lyon', 'France', 'Espagne', 'FIFA World Cup', TO_DATE('06/16/2016', 'MM/DD/YYYY'));
COMMIT TRANSACTION;
-- FIFA WORLD CUP 2012
BEGIN TRANSACTION;
INSERT INTO Match VALUES(112, TO_DATE('05/13/2012', 'MM/DD/YYYY'), '199-37-4485', '862-57-0108', '846-18-4682', '358-45-0143', 'Berlin', 'Allemagne', 'Espagne', 'FIFA World Cup', TO_DATE('05/13/2012', 'MM/DD/YYYY'));
INSERT INTO Match VALUES(212, TO_DATE('05/14/2012', 'MM/DD/YYYY'), '828-42-3900', '628-77-4428', '218-60-1618', '540-08-1530', 'Hambourg', 'Italie', 'France', 'FIFA World Cup', TO_DATE('05/13/2012', 'MM/DD/YYYY'));
INSERT INTO Match VALUES(312, TO_DATE('05/16/2012', 'MM/DD/YYYY'), '267-36-2014', '731-55-0420', '894-47-8550', '623-80-5525', 'Munich', 'Espagne', 'Italie', 'FIFA World Cup', TO_DATE('05/13/2012', 'MM/DD/YYYY'));
INSERT INTO Match VALUES(412, TO_DATE('05/17/2012', 'MM/DD/YYYY'), '777-92-0130', '168-55-4560', '587-83-0024', '183-45-6549', 'Cologne', 'France', 'Allemagne', 'FIFA World Cup', TO_DATE('05/13/2012', 'MM/DD/YYYY'));
INSERT INTO Match VALUES(512, TO_DATE('05/19/2012', 'MM/DD/YYYY'), '358-45-0143', '846-18-4682', '628-77-4428', '828-42-3900', 'Stuttgart', 'Allemagne', 'Italie', 'FIFA World Cup', TO_DATE('05/13/2012', 'MM/DD/YYYY'));
INSERT INTO Match VALUES(612, TO_DATE('05/20/2012', 'MM/DD/YYYY'), '540-08-1530', '218-60-1618', '862-57-0108', '199-37-4485', 'Berlin', 'France', 'Luxembourg', 'FIFA World Cup', TO_DATE('05/13/2012', 'MM/DD/YYYY'));
COMMIT TRANSACTION;


--FIFA WORLD CUP 2016 : MATCH 1  ------------------------------------------------------------------------------
-- Equipe de France
BEGIN TRANSACTION;
INSERT INTO estTitulaire VALUES(116,'672-21-6357'); -- gardien
INSERT INTO estTitulaire VALUES(116,'517-99-7315'); -- attaquant 1
INSERT INTO estTitulaire VALUES(116,'598-69-9395'); -- attaquant 2
INSERT INTO estTitulaire VALUES(116,'801-58-7116'); -- attaquant 3
INSERT INTO estTitulaire VALUES(116,'218-43-2649'); -- defenseur 1
INSERT INTO estTitulaire VALUES(116,'759-56-1123'); -- defenseur 2
INSERT INTO estTitulaire VALUES(116,'712-47-4551'); -- defenseur 3
INSERT INTO estTitulaire VALUES(116,'676-46-6361'); -- defenseur 4
INSERT INTO estTitulaire VALUES(116,'554-84-9816'); -- milieu 1
INSERT INTO estTitulaire VALUES(116,'713-80-8045'); -- milieu 2
INSERT INTO estTitulaire VALUES(116,'109-92-5951'); -- milieu 3
COMMIT TRANSACTION;

-- Equipe de France
BEGIN TRANSACTION;
INSERT INTO estRemplacant VALUES(116,'222-17-6588'); -- gardien 2
INSERT INTO estRemplacant VALUES(116,'309-33-3100'); -- attaquant 4
INSERT INTO estRemplacant VALUES(116,'479-74-4091'); -- defenseur 5
INSERT INTO estRemplacant VALUES(116,'598-57-3552'); -- defenseur 6
INSERT INTO estRemplacant VALUES(116,'491-88-3651'); -- milieu 4
COMMIT TRANSACTION;

-- Equipe d'Italie
BEGIN TRANSACTION;
INSERT INTO estTitulaire VALUES(116,'239-94-2481'); -- gardien
INSERT INTO estTitulaire VALUES(116,'628-35-6043'); -- attaquant 1
INSERT INTO estTitulaire VALUES(116,'481-98-9388'); -- attaquant 2
INSERT INTO estTitulaire VALUES(116,'813-14-3836'); -- attaquant 3
INSERT INTO estTitulaire VALUES(116,'528-86-5382'); -- defenseur 1
INSERT INTO estTitulaire VALUES(116,'852-68-6050'); -- defenseur 2
INSERT INTO estTitulaire VALUES(116,'220-64-9061'); -- defenseur 3
INSERT INTO estTitulaire VALUES(116,'413-44-4776'); -- defenseur 4
INSERT INTO estTitulaire VALUES(116,'293-65-6353'); -- milieu 1
INSERT INTO estTitulaire VALUES(116,'181-45-3926'); -- milieu 2
INSERT INTO estTitulaire VALUES(116,'121-45-4593'); -- milieu 3
COMMIT TRANSACTION;

-- Equipe d'Italie
BEGIN TRANSACTION;
INSERT INTO estRemplacant VALUES(116,'296-53-5721'); -- gardien 2
INSERT INTO estRemplacant VALUES(116,'212-76-7447'); -- attaquant 4
INSERT INTO estRemplacant VALUES(116,'450-86-7182'); -- defenseur 5
INSERT INTO estRemplacant VALUES(116,'619-86-3630'); -- defenseur 6
INSERT INTO estRemplacant VALUES(116,'461-74-8221'); -- milieu 4
COMMIT TRANSACTION;

BEGIN TRANSACTION;
-- Equipe de France
INSERT INTO But VALUES(26, 116, 'classique', '672-21-6357', NULL);
INSERT INTO But VALUES(49, 116, 'corner', '598-69-9395','554-84-9816');
-- Equipe d'Italie
INSERT INTO But VALUES(78, 116, 'penalty', '628-35-6043',NULL);
REFRESH MATERIALIZED VIEW MVclassementButeurFIFAWorldCup2016;
REFRESH MATERIALIZED VIEW MVclassementButeurFIFAWorldCup2012;
REFRESH MATERIALIZED VIEW MVclassementPasseurFIFAWorldCup2016;
REFRESH MATERIALIZED VIEW MVclassementPasseurFIFAWorldCup2012;
REFRESH MATERIALIZED VIEW MVclassementEquipeTousChampionnats;
REFRESH MATERIALIZED VIEW MVclassementEquipeFIFAWorldCup2016;
REFRESH MATERIALIZED VIEW MVclassementEquipeAdomicile;
REFRESH MATERIALIZED VIEW MVclassementEquipeAlexterieur;
COMMIT TRANSACTION;

BEGIN TRANSACTION;
-- Equipe de France
INSERT INTO ChangementDeJoueur VALUES(54, 116, '309-33-3100', '517-99-7315');
INSERT INTO ChangementDeJoueur VALUES(21, 116, '479-74-4091', '676-46-6361');
-- Equipe d'Italie
INSERT INTO ChangementDeJoueur VALUES(45, 116, '212-76-7447', '481-98-9388');
COMMIT TRANSACTION;

INSERT INTO Sanction VALUES (43, 116, 'avertissement','598-69-9395');


--FIFA WORLD CUP 2016 : MATCH 2  ------------------------------------------------------------------------------
-- Equipe de Espagne
BEGIN TRANSACTION;
INSERT INTO estTitulaire VALUES (216, '156-66-7042'); --gardien
INSERT INTO estTitulaire VALUES (216, '572-23-0440'); --attaquant 1
INSERT INTO estTitulaire VALUES (216, '223-06-2131'); --attaquant 2
INSERT INTO estTitulaire VALUES (216, '116-85-1811'); --attaquant 3
INSERT INTO estTitulaire VALUES (216, '197-76-4764'); --milieu 1
INSERT INTO estTitulaire VALUES (216, '263-29-5955'); --milieu 2
INSERT INTO estTitulaire VALUES (216, '758-13-1183'); --milieu 3
INSERT INTO estTitulaire VALUES (216, '599-34-2166'); --defenseur 1
INSERT INTO estTitulaire VALUES (216, '479-70-1380'); --defenseur 2
INSERT INTO estTitulaire VALUES (216, '758-81-4253'); --defenseur 3
INSERT INTO estTitulaire VALUES (216, '774-27-1632'); --defenseur 4
COMMIT TRANSACTION;

-- Equipe de Espagne
BEGIN TRANSACTION;
INSERT INTO estRemplacant VALUES (216, '634-17-7252'); --gardien 2
INSERT INTO estRemplacant VALUES (216, '150-37-4420'); --gardien 3
INSERT INTO estRemplacant VALUES (216, '225-34-7067'); --defenseur 5
INSERT INTO estRemplacant VALUES (216, '557-13-3095'); --attaquant 4
INSERT INTO estRemplacant VALUES (216, '693-45-3640'); --defenseur 6
COMMIT TRANSACTION;

-- Equipe de Allemagne
BEGIN TRANSACTION;
INSERT INTO estTitulaire VALUES (216, '355-04-9975'); --gardien
INSERT INTO estTitulaire VALUES (216, '647-70-2710'); --defenseur 1
INSERT INTO estTitulaire VALUES (216, '347-70-4084'); --defenseur 2
INSERT INTO estTitulaire VALUES (216, '613-59-2911'); --defenseur 3
INSERT INTO estTitulaire VALUES (216, '811-19-6435'); --milieu 1
INSERT INTO estTitulaire VALUES (216, '734-41-1490'); --milieu 2
INSERT INTO estTitulaire VALUES (216, '295-32-9852'); --milieu 3
INSERT INTO estTitulaire VALUES (216, '482-29-8334'); --attaquant 1
INSERT INTO estTitulaire VALUES (216, '389-95-2189'); --attaquant 2
INSERT INTO estTitulaire VALUES (216, '375-68-3569'); --attaquant 3
INSERT INTO estTitulaire VALUES (216, '709-85-9048'); --attaquant 4
COMMIT TRANSACTION;

-- Equipe de Allemagne--
BEGIN TRANSACTION;
INSERT INTO estRemplacant VALUES (216, '343-37-2913'); --defenseur 4
INSERT INTO estRemplacant VALUES (216, '392-53-4416'); --defenseur 5
INSERT INTO estRemplacant VALUES (216, '815-86-6942'); --attaquant 5
INSERT INTO estRemplacant VALUES (216, '109-26-1686'); --milieu 4
INSERT INTO estRemplacant VALUES (216, '771-30-3457'); --gardien 2
COMMIT TRANSACTION;

BEGIN TRANSACTION;
-- Equipe de Espagne
INSERT INTO But VALUES(30, 216, 'classique', '223-06-2131', '758-13-1183');
INSERT INTO But VALUES(89, 216, 'penalty', '572-23-0440',NULL);
-- Equipe d'Allemagne
INSERT INTO But VALUES(15, 216, 'coup franc', '482-29-8334',NULL);
INSERT INTO But VALUES(70, 216, 'penalty', '815-86-6942',NULL);
REFRESH MATERIALIZED VIEW MVclassementButeurFIFAWorldCup2016;
REFRESH MATERIALIZED VIEW MVclassementButeurFIFAWorldCup2012;
REFRESH MATERIALIZED VIEW MVclassementPasseurFIFAWorldCup2016;
REFRESH MATERIALIZED VIEW MVclassementPasseurFIFAWorldCup2012;
REFRESH MATERIALIZED VIEW MVclassementEquipeTousChampionnats;
REFRESH MATERIALIZED VIEW MVclassementEquipeFIFAWorldCup2016;
REFRESH MATERIALIZED VIEW MVclassementEquipeAdomicile;
REFRESH MATERIALIZED VIEW MVclassementEquipeAlexterieur;
COMMIT TRANSACTION;

BEGIN TRANSACTION;
-- Equipe de Espagne
INSERT INTO ChangementDeJoueur VALUES(60, 216, '557-13-3095', '223-06-2131');
INSERT INTO ChangementDeJoueur VALUES(70, 216, '225-34-7067', '599-34-2166');
-- Equipe d'Allemagne
INSERT INTO ChangementDeJoueur VALUES(45, 216, '613-59-2911', '109-26-1686');
INSERT INTO ChangementDeJoueur VALUES(62, 216, '482-29-8334', '815-86-6942');
COMMIT TRANSACTION;

BEGIN TRANSACTION;
-- Equipe de Espagne
INSERT INTO Sanction VALUES (43, 216, 'avertissement','774-27-1632');
INSERT INTO Sanction VALUES (87, 216, 'avertissement','479-70-1380');
-- Equipe d'Allemagne
INSERT INTO Sanction VALUES (55, 216, 'avertissement','647-70-2710');
COMMIT TRANSACTION;



-- FIFA WORLD CUP 2012 ------------------------------------------------------------------------------
-- Equipe de France
BEGIN TRANSACTION;
INSERT INTO estTitulaire VALUES (612, '672-21-6357'); --gardien
INSERT INTO estTitulaire VALUES (612, '517-99-7315'); --attaquant 1
INSERT INTO estTitulaire VALUES (612, '598-69-9395'); --attaquant 2
INSERT INTO estTitulaire VALUES (612, '116-85-1811'); --attaquant 3
INSERT INTO estTitulaire VALUES (612, '572-54-4116'); --milieu 1
INSERT INTO estTitulaire VALUES (612, '554-84-9816'); --milieu 2
INSERT INTO estTitulaire VALUES (612, '537-75-2846'); --milieu 3
INSERT INTO estTitulaire VALUES (612, '218-43-2649'); --defenseur 1
INSERT INTO estTitulaire VALUES (612, '712-47-4551'); --defenseur 2
INSERT INTO estTitulaire VALUES (612, '676-46-6361'); --defenseur 3
INSERT INTO estTitulaire VALUES (612, '824-24-4988'); --defenseur 4
COMMIT TRANSACTION;
-- Equipe du Luxembourg
BEGIN TRANSACTION;
INSERT INTO estTitulaire VALUES(612,'865-57-1952'); -- gardien
INSERT INTO estTitulaire VALUES(612,'159-75-3456'); -- attaquant 1
INSERT INTO estTitulaire VALUES(612,'438-86-7894'); -- attaquant 2
INSERT INTO estTitulaire VALUES(612,'456-96-7894'); -- milieu 4
INSERT INTO estTitulaire VALUES(612,'429-34-2502'); -- defenseur 1
INSERT INTO estTitulaire VALUES(612,'197-96-6845'); -- defenseur 2
INSERT INTO estTitulaire VALUES(612,'934-55-6548'); -- defenseur 3
INSERT INTO estTitulaire VALUES(612,'435-39-6458'); -- defenseur 4
INSERT INTO estTitulaire VALUES(612,'423-12-7542'); -- milieu 1
INSERT INTO estTitulaire VALUES(612,'147-85-7456'); -- milieu 2
INSERT INTO estTitulaire VALUES(612,'496-12-6482'); -- milieu 3
COMMIT TRANSACTION;


-- Equipe de France
BEGIN TRANSACTION;
INSERT INTO estRemplacant VALUES(612,'222-17-6588'); -- gardien 2
INSERT INTO estRemplacant VALUES(612,'612-35-2938'); -- attaquant 4
INSERT INTO estRemplacant VALUES(612,'479-74-4091'); -- defenseur 5
INSERT INTO estRemplacant VALUES(612,'598-57-3552'); -- defenseur 6
INSERT INTO estRemplacant VALUES(612,'713-80-8045'); -- milieu 5
COMMIT TRANSACTION;
-- Equipe Luxembourg
BEGIN TRANSACTION;
INSERT INTO estRemplacant VALUES(612,'456-77-1234'); -- gardien 2
INSERT INTO estRemplacant VALUES(612,'486-74-1963'); -- defenseur 4
INSERT INTO estRemplacant VALUES(612,'863-96-7638'); -- milieu 5
COMMIT TRANSACTION;


BEGIN TRANSACTION;
-- Equipe du Luxembourg
INSERT INTO But VALUES(12, 612, 'classique', '438-86-7894', '423-12-7542');
INSERT INTO But VALUES(77, 612, 'penalty', '438-86-7894',NULL);
-- Equipe de France
INSERT INTO But VALUES(15, 612, 'coup franc', '672-21-6357',NULL);
REFRESH MATERIALIZED VIEW MVclassementButeurFIFAWorldCup2016;
REFRESH MATERIALIZED VIEW MVclassementButeurFIFAWorldCup2012;
REFRESH MATERIALIZED VIEW MVclassementPasseurFIFAWorldCup2016;
REFRESH MATERIALIZED VIEW MVclassementPasseurFIFAWorldCup2012;
REFRESH MATERIALIZED VIEW MVclassementEquipeTousChampionnats;
REFRESH MATERIALIZED VIEW MVclassementEquipeFIFAWorldCup2016;
REFRESH MATERIALIZED VIEW MVclassementEquipeAdomicile;
REFRESH MATERIALIZED VIEW MVclassementEquipeAlexterieur;
COMMIT TRANSACTION;

BEGIN TRANSACTION;
-- Equipe de France
INSERT INTO ChangementDeJoueur VALUES(60, 612, '222-17-6588', '672-21-6357');
INSERT INTO ChangementDeJoueur VALUES(70, 612, '713-80-8045', '572-54-4116');
-- Equipe Luxembourg
INSERT INTO ChangementDeJoueur VALUES(45, 612, '486-74-1963', '429-34-2502');
INSERT INTO ChangementDeJoueur VALUES(61, 612, '863-96-7638', '456-96-7894');
COMMIT TRANSACTION;

BEGIN TRANSACTION;
-- Equipe du Luxembourg
INSERT INTO Sanction VALUES (43, 612, 'avertissement','865-57-1952');
INSERT INTO Sanction VALUES (87, 612, 'avertissement','147-85-7456');
-- Equipe de France
INSERT INTO Sanction VALUES (55, 612, 'avertissement','517-99-7315');
COMMIT TRANSACTION;