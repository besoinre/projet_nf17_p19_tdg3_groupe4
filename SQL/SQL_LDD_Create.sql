DROP VIEW IF EXISTS vClassementNbButsFIFAWorldCup2016;
DROP VIEW IF EXISTS vNbButsEquipeFIFAWorldCup2016;

DROP MATERIALIZED VIEW IF EXISTS MVclassementEquipeAlexterieur;
DROP MATERIALIZED VIEW IF EXISTS MVclassementEquipeAdomicile;
DROP MATERIALIZED VIEW IF EXISTS MVclassementEquipeFIFAWorldCup2016;
DROP MATERIALIZED VIEW IF EXISTS MVclassementEquipeTousChampionnats;

DROP MATERIALIZED VIEW IF EXISTS MVclassementPasseurFIFAWorldCup2012;
DROP MATERIALIZED VIEW IF EXISTS MVclassementPasseurFIFAWorldCup2016;
DROP MATERIALIZED VIEW IF EXISTS MVclassementButeurFIFAWorldCup2012;
DROP MATERIALIZED VIEW IF EXISTS MVclassementButeurFIFAWorldCup2016;


DROP TABLE IF EXISTS ChangementDeJoueur;
DROP TABLE IF EXISTS Sanction;
DROP TABLE IF EXISTS But;
DROP TABLE IF EXISTS estRemplacant;
DROP TABLE IF EXISTS estTitulaire;
DROP TABLE IF EXISTS Match;
DROP TABLE IF EXISTS Contrat_sponsor;
DROP TABLE IF EXISTS Sponsor;
DROP TABLE IF EXISTS Participant;
DROP TABLE IF EXISTS Championnat;
DROP TABLE IF EXISTS Arbitre;
DROP TABLE IF EXISTS Joueur;
DROP TABLE IF EXISTS SurnomEquipe;
DROP TABLE IF EXISTS Equipe;
DROP TABLE IF EXISTS Entraineur;
DROP TABLE IF EXISTS Ville;




CREATE TABLE Ville(
	nom varchar(20) PRIMARY KEY,
	pays varchar(20) NOT NULL,
	nombreDeStades int NOT NULL,
	population int
);

CREATE TABLE Entraineur(
	numSecu varchar(15) PRIMARY KEY,
	nom varchar(20) NOT NULL,
	prenom varchar(20) NOT NULL,
	dateDeNaissance DATE NOT NULL
);

CREATE TABLE Equipe(
	nom varchar(20) PRIMARY KEY,
	ville varchar(20) REFERENCES Ville(nom) NOT NULL,
	entraineur varchar(15) NOT NULL REFERENCES Entraineur(numSecu)
);

CREATE TABLE SurnomEquipe(
	nom varchar(20) PRIMARY KEY,
	equipe varchar(20) REFERENCES Equipe(nom) NOT NULL
);

CREATE TABLE Joueur(
	numSecu varchar(15) PRIMARY KEY,
	nom varchar(20) NOT NULL,
	prenom varchar(20) NOT NULL,
	dateDeNaissance DATE NOT NULL,
	surnom varchar(20),
	poste varchar(20) check (poste in ('gardien', 'defenseur', 'milieu', 'attaquant')),
	experience int,
	equipe varchar(20) REFERENCES Equipe(nom),
        CONSTRAINT ck_experience CHECK (experience IS NOT NULL AND date_part('year', age(dateDeNaissance)) > experience)
);

CREATE TABLE Arbitre(
	numSecu varchar(15) PRIMARY KEY,
	nom varchar(20) NOT NULL,
	prenom varchar(20) NOT NULL,
	dateDeNaissance DATE NOT NULL
);

CREATE TABLE Championnat(
	nom varchar(20),
	dateDebut date,
	dateFin date NOT NULL,
	organisateur varchar(20) NOT NULL,
	PRIMARY KEY (nom, dateDebut),
	CONSTRAINT datesChampionnat CHECK (dateDebut < dateFin)
);

CREATE TABLE Participant(
	nomChampionnat varchar(20),
	dateChampionnat date,
	equipe varchar(20) REFERENCES Equipe(nom),
	FOREIGN KEY (nomChampionnat, dateChampionnat) REFERENCES Championnat(nom, dateDebut),
	PRIMARY KEY(nomChampionnat, dateChampionnat, equipe)
);

CREATE TABLE Sponsor(
	nom varchar(20) PRIMARY KEY,
	importance int NOT NULL CHECK (importance BETWEEN 0 AND 10)
);

CREATE TABLE Contrat_sponsor(
	nomSponsor varchar(20) REFERENCES Sponsor(nom),		
	nomChampionnat varchar(20),
	dateChampionnat date,
	FOREIGN KEY (nomChampionnat, dateChampionnat) REFERENCES Championnat(nom, dateDebut), 
	PRIMARY KEY (nomSponsor, nomChampionnat, dateChampionnat)
);

CREATE TABLE Match(
	idMatch int PRIMARY KEY,
	dateMatch Date NOT NULL,
	arbitreCentral varchar(15) REFERENCES Arbitre(numSecu) NOT NULL,
	arbitreAssistantUn varchar(15) REFERENCES Arbitre(numSecu) NOT NULL,
	arbitreAssistantDeux varchar(15) REFERENCES Arbitre(numSecu) NOT NULL,
	arbitreRemplacant varchar(15) REFERENCES Arbitre(numSecu) NOT NULL,
	ville varchar(20) REFERENCES Ville(nom) NOT NULL,
	equipeLocale varchar(20) REFERENCES Equipe(nom) NOT NULL,
	equipeVisiteur varchar(20) REFERENCES Equipe(nom) NOT NULL,
	nomChampionnat varchar(20) NOT NULL,
	dateChampionnat date NOT NULL,
	FOREIGN KEY (nomChampionnat, dateChampionnat) REFERENCES Championnat(nom, dateDebut),
	CONSTRAINT matchArbitreCA1 CHECK (arbitreCentral != arbitreAssistantUn),
	CONSTRAINT matchArbitreCA2 CHECK (arbitreCentral != arbitreAssistantDeux),
	CONSTRAINT matchArbitreCR CHECK (arbitreCentral != arbitreRemplacant),
	CONSTRAINT matchArbitreA1A2 CHECK (arbitreAssistantUn != arbitreAssistantDeux),
	CONSTRAINT matchArbitreA1R CHECK (arbitreAssistantUn != arbitreRemplacant),
	CONSTRAINT matchArbitreA2R CHECK (arbitreAssistantDeux != arbitreRemplacant),
	CONSTRAINT matchLocalVisiteur CHECK (equipeLocale != equipeVisiteur)
);

CREATE TABLE estTitulaire(
        idmatch int REFERENCES Match(idMatch),
        titulaire VARCHAR(15) REFERENCES Joueur(numSecu),
        PRIMARY KEY(idmatch, titulaire)
);

CREATE TABLE estRemplacant(
        idmatch int REFERENCES Match(idMatch),
        remplacant VARCHAR(15) REFERENCES Joueur(numSecu),
        PRIMARY KEY(idmatch, remplacant)
);

CREATE TABLE But(
    minute int,
    idMatch int REFERENCES Match(idMatch),
    type varchar(15) CHECK (type in ('penalty', 'coup franc', 'corner', 'classique')) NOT NULL,
    numSecuButeur varchar(15) REFERENCES Joueur(numSecu) NOT NULL,
    numSecuPasseur varchar(15) REFERENCES Joueur(numSecu),
    PRIMARY KEY(minute, idMatch),
    CONSTRAINT limiteMinute CHECK(minute <= 120 AND minute >= 0)
);



CREATE TABLE Sanction(
    minute int,
    idMatch int REFERENCES Match(idMatch),
    type varchar(15) CHECK (type in ('avertissement', 'expulsion directe', 'expulsion indirecte')) NOT NULL,
    numSecusSanctionne varchar(15) REFERENCES Joueur(numSecu) NOT NULL,
    PRIMARY KEY(minute,idMatch),
    CONSTRAINT limiteMinute CHECK(minute <= 120 AND minute >= 0)
);



CREATE TABLE ChangementDeJoueur(
	minute int,
	idMatch int REFERENCES Match(idMatch),
	numSecuJoueurIn VARCHAR(15) REFERENCES Joueur(numSecu) NOT NULL,
	numSecuJoueurOut VARCHAR(15) REFERENCES Joueur(numSecu) NOT NULL,
	CHECK(numSecuJoueurIn !=numSecuJoueurOut),
	PRIMARY KEY(minute,idMatch),
	CONSTRAINT limiteMinute CHECK(minute <= 120 AND minute >= 0) 
);





--
-- Ajouts pour répondre aux fonctionnalitées demandées
--



-- Fonctionnalités : Niveau 1
-----------------------------

----------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Partie Utilisateur ----------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------

--
-- CLASSEMENT MEILLEURS BUTEURS
--

-- MV = MATERIALIZED VIEW

-- Le classement des meilleurs buteurs du championnat : FIFA World Cup 2016
CREATE MATERIALIZED VIEW MVclassementButeurFIFAWorldCup2016 AS
SELECT J.numSecu, J.nom, J.prenom, J.equipe, COUNT(B.numSecuButeur) as nbButs
FROM Joueur J
JOIN But B
ON J.numSecu = B.numSecuButeur
JOIN Match M
ON B.idMatch = M.idMatch
WHERE M.nomChampionnat = 'FIFA World Cup'
AND M.dateChampionnat = TO_DATE('06/16/2016', 'MM/DD/YYYY')
GROUP BY J.numSecu
ORDER BY nbButs DESC;

-- Le classement des meilleurs buteurs du championnat : FIFA World Cup 2012
CREATE MATERIALIZED VIEW MVclassementButeurFIFAWorldCup2012 AS
SELECT J.numSecu, J.nom, J.prenom, J.equipe, COUNT(B.numSecuButeur) as nbButs
FROM Joueur J
JOIN But B
ON J.numSecu = B.numSecuButeur
JOIN Match M
ON B.idMatch = M.idMatch
WHERE M.nomChampionnat = 'FIFA World Cup'
AND M.dateChampionnat = TO_DATE('05/13/2012', 'MM/DD/YYYY')
GROUP BY J.numSecu
ORDER BY nbButs DESC;


-- Le classement des meilleurs passeurs du championnat : FIFA World Cup 2016
CREATE MATERIALIZED VIEW MVclassementPasseurFIFAWorldCup2016 AS
SELECT J.numSecu, J.nom, J.prenom, J.equipe, COUNT(B.numSecuButeur) as nbButs
FROM Joueur J
JOIN But B
ON J.numSecu = B.numSecuPasseur
JOIN Match M
ON B.idMatch = M.idMatch
WHERE M.nomChampionnat = 'FIFA World Cup'
AND M.dateChampionnat = TO_DATE('06/16/2016', 'MM/DD/YYYY')
GROUP BY J.numSecu
ORDER BY nbButs DESC;

-- Le classement des meilleurs passeurs du championnat : FIFA World Cup 2012
CREATE MATERIALIZED VIEW MVclassementPasseurFIFAWorldCup2012 AS
SELECT J.numSecu, J.nom, J.prenom, J.equipe, COUNT(B.numSecuButeur) as nbButs
FROM Joueur J
JOIN But B
ON J.numSecu = B.numSecuPasseur
JOIN Match M
ON B.idMatch = M.idMatch
WHERE M.nomChampionnat = 'FIFA World Cup'
AND M.dateChampionnat = TO_DATE('05/13/2012', 'MM/DD/YYYY')
GROUP BY J.numSecu
ORDER BY nbButs DESC;


--
-- CLASSEMENT DES EQUIPES
--

-- Le classement des equipes tous championnats confondus
CREATE MATERIALIZED VIEW MVclassementEquipeTousChampionnats AS
SELECT J.equipe, COUNT(J.equipe) as nbButs
FROM But B
JOIN Match M
ON B.idMatch = M.idMatch
JOIN Joueur J
ON B.numSecuButeur = J.numSecu
GROUP BY J.equipe
ORDER BY nbButs DESC;

-- Le classement des equipes dans le championnat : FIFA World Cup 2016
CREATE MATERIALIZED VIEW MVclassementEquipeFIFAWorldCup2016 AS
SELECT J.equipe, COUNT(J.equipe) as nbButs
FROM But B
JOIN Match M
ON B.idMatch = M.idMatch
JOIN Joueur J
ON B.numSecuButeur = J.numSecu
WHERE M.nomChampionnat = 'FIFA World Cup'
AND M.dateChampionnat = TO_DATE('06/16/2016', 'MM/DD/YYYY')
GROUP BY J.equipe
ORDER BY nbButs DESC;

-- Le classement des equipes les plus fortes à domicile tous championnats confondus
CREATE MATERIALIZED VIEW MVclassementEquipeAdomicile AS
SELECT J.equipe as equipeMatchLocale, COUNT(J.equipe) as nbButs
FROM But B
JOIN Match M
ON B.idMatch = M.idMatch
JOIN Joueur J
ON B.numSecuButeur = J.numSecu
WHERE M.equipeLocale = J.equipe
GROUP BY J.equipe
ORDER BY nbButs DESC;

-- Le classement des equipes les plus fortes à l'exterieur tous championnats confondus
CREATE MATERIALIZED VIEW MVclassementEquipeAlexterieur AS
SELECT J.equipe as equipeMatchVisiteur, COUNT(J.equipe) as nbButs
FROM But B
JOIN Match M
ON B.idMatch = M.idMatch
JOIN Joueur J
ON B.numSecuButeur = J.numSecu
WHERE M.equipeVisiteur = J.equipe
GROUP BY J.equipe
ORDER BY nbButs DESC;




-- Fonctionnalités : Niveau 2
-----------------------------

-- WORK IN PROGRESS

-- On recupere le nombre de buts des équipes dans un championnat, ici FIFA World Cup 2016
CREATE VIEW vNbButsEquipeFIFAWorldCup2016 AS
SELECT equipe, COUNT(equipe) as nbButs
FROM Match M, But B, Joueur J
WHERE M.idMatch=B.idMatch
AND numSecuButeur=J.numSecu
AND M.nomChampionnat = 'FIFA World Cup'
AND dateChampionnat = TO_DATE('06/16/2016', 'MM/DD/YYYY')
GROUP BY equipe;

CREATE VIEW vClassementNbButsFIFAWorldCup2016 AS
SELECT P.nomChampionnat, P.dateChampionnat, P.equipe, nbButs
FROM Participant P
LEFT JOIN vNbButsEquipeFIFAWorldCup2016 view
ON P.equipe = view.equipe
WHERE nomChampionnat = 'FIFA World Cup'
AND dateChampionnat = TO_DATE('06/16/2016', 'MM/DD/YYYY')
ORDER BY nbButs DESC;


