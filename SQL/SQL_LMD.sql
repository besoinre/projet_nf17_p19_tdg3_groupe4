--
-- SQL LMD
--
-- Clément Mouchet
-- Florent Gaudin
-- Rémi Besoin
-- Xuefeng Hu
-- Zilu Zheng
--



--
-- Fonctionnalités : Niveau 1
--
----------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Partie Administrateur ----------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Création d'un utilisateur Admin qui aura tous les droits
CREATE USER Admin;

GRANT ALL PRIVILEGES
ON Ville, Entraineur, Equipe, SurnomEquipe, Joueur, Arbitre, Championnat, Participant, Sponsor, Contrat_sponsor, Match, estTitulaire, EstRemplacant, But, Sanction, ChangementDeJoueur
TO Admin;


--1)
-- Ajout d'un joueur
INSERT INTO Joueur VALUES ('957-534-0304', 'Ronaldo', 'Cristiano',TO_DATE('02/05/1985', 'MM/DD/YYYY'), null,'attaquant', 12, 'Portugal');
-- Modification d'un joueur
UPDATE Joueur
SET surnom= 'C Ronaldo'
WHERE numSecu='957-534-0304';
-- Suppression d'un joueur
DELETE FROM Joueur
WHERE numSecu='957-534-0304';


--2)
-- Ajout d'un entraîneur
INSERT INTO Entraineur VALUES ('476-382-9173', 'DAVID', 'Jean', TO_DATE('03/23/1994', 'MM/DD/YYYY'));
-- Modification d'un entraîneur
UPDATE Entraineur
SET nom='David'
WHERE numSecu='476-382-9173';
-- Suppression d'un entraîneur
DELETE FROM Entraineur
WHERE numSecu='476-382-9173';

--3)
-- Ajout d'un arbitre
INSERT INTO Arbitre VALUES ('249-657-9706', 'Galler', 'Joey', TO_DATE('06/12/1970', 'MM/DD/YYYY'));
-- Modification d'un arbitre
UPDATE Arbitre
SET nom='Bing'
WHERE numSecu='249-657-9706';
-- Suppression d'un arbitre
DELETE FROM Arbitre
WHERE numSecu='249-657-9706';

--4)
-- Ajout d'une equipe
INSERT INTO Equipe VALUES ('Chine', 'Pekin', '582-44-7607');
-- Modification d'une equipe
UPDATE Equipe
SET entraineur='365-46-1173'
WHERE nom='Chine';
-- Suppression d'une equipe
DELETE FROM Equipe
WHERE nom='Chine';

--5)
-- Ajout d'un championnat
INSERT INTO Championnat VALUES('FIFA World Cup', TO_DATE('05/13/2020', 'MM/DD/YYYY'), TO_DATE('06/14/2020', 'MM/DD/YYYY'), 'FIFA');
-- Modification d'un championnat
UPDATE Championnat
SET dateFin= TO_DATE('06/20/2020','MM/DD/YYYY')
WHERE (nom, dateDebut) = ('FIFA World Cup', TO_DATE('05/13/2020', 'MM/DD/YYYY'));
-- Suppression d'un championnat
DELETE FROM Championnat
WHERE (nom, dateDebut) = ('FIFA World Cup', TO_DATE('05/13/2020', 'MM/DD/YYYY'));

--6)
-- Ajout d'un match
INSERT INTO Match VALUES(716, TO_DATE('06/24/2016', 'MM/DD/YYYY'), '894-47-8550', '540-08-1530', '358-45-0143', '199-37-4485', 'Berlin', 'Allemagne', 'Portugal', 'FIFA World Cup', TO_DATE('06/16/2016', 'MM/DD/YYYY'));
-- Modification d'un match
UPDATE Match
SET ville= 'Lyon'
WHERE idMatch= 716;
-- Suppression d'un match
DELETE FROM match
WHERE idMatch= 716;

--7)
-- Modification d'un joueur titulaire
BEGIN TRANSACTION;
DELETE FROM estTitulaire
WHERE (idmatch, titulaire)= (116,'517-99-7315');
INSERT INTO estTitulaire VALUES (116,'612-35-2938');
COMMIT TRANSACTION;

--8)
-- Modification d'un joueur remplacant
BEGIN TRANSACTION;
DELETE FROM EstRemplacant
WHERE (idmatch, remplacant)= (116,'222-17-6588');
INSERT INTO estRemplacant VALUES (116,'334-03-3194');
COMMIT TRANSACTION;

--9)
-- Ajout d'un but dans un match
INSERT INTO But VALUES(21, 116, 'classique', '517-99-7315', NULL);
-- Modification d'un but dans un match
UPDATE But
SET numSecuPasseur= '109-92-5951'
WHERE (minute, idMatch)= (21,116);
-- Suppression d'un but dans un match
DELETE FROM But
WHERE (minute, idMatch)= (21,116);

--10)
-- Ajout d'une sanction dans un match
INSERT INTO Sanction VALUES(33, 116, 'avertissement', '517-99-7315');
-- Modification d'une sanction dans un match
UPDATE Sanction
SET numSecusSanctionne= '554-84-9816'
WHERE (minute, idMatch)= (33,116);
-- Suppression d'une sanction dans un match
DELETE FROM Sanction
WHERE (minute, idMatch)= (33,116);

--11)
-- Ajout d'un changement de joueur dans un match
INSERT INTO ChangementDeJoueur VALUES(90, 116, '222-17-6588', '672-21-6357');
-- Modification d'un changement de joueur dans un match
UPDATE ChangementDeJoueur
SET numSecuJoueurIn= '491-88-3651'
WHERE (minute,idMatch)= (90,116);
-- Suppression d'un changement de joueur dans un match
DELETE FROM ChangementDeJoueur
WHERE (minute,idMatch)= (90,116);


----------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Partie Utilisateur ----------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Création d'un utilisateur Utilisateur qui aura seulement les droits en lecture
CREATE USER Utilisateur;

GRANT SELECT
ON Ville, Entraineur, Equipe, SurnomEquipe, Joueur, Arbitre, Championnat, Participant, Sponsor, Contrat_sponsor, Match, estTitulaire, EstRemplacant, But, Sanction, ChangementDeJoueur
TO Utilisateur;

--
-- CLASSEMENT MEILLEURS BUTEURS
--

-- MV = MATERIALIZED VIEW
-- Les créations des MV sont dans SQL_LDD_Create.sql

-- Le classement des meilleurs buteurs du championnat : FIFA World Cup 2016
SELECT * FROM MVclassementButeurFIFAWorldCup2016;

-- Le classement des meilleurs buteurs du championnat : FIFA World Cup 2012
SELECT * FROM MVclassementButeurFIFAWorldCup2012;

-- Le classement des meilleurs passeurs du championnat : FIFA World Cup 2016
SELECT * FROM MVclassementPasseurFIFAWorldCup2016;

-- Le classement des meilleurs passeurs du championnat : FIFA World Cup 2012
SELECT * FROM MVclassementPasseurFIFAWorldCup2012;

-- Le classement des meilleurs buteurs du championnat FIFA World Cup 2016 sur la période : 15 juin 2016 - 17 juin 2016
SELECT J.numSecu, J.nom, J.prenom, J.equipe, COUNT(B.numSecuButeur) as nbButs
FROM Joueur J
JOIN But B
ON J.numSecu = B.numSecuButeur
JOIN Match M
ON B.idMatch = M.idMatch
WHERE M.nomChampionnat = 'FIFA World Cup'
AND M.dateChampionnat = TO_DATE('06/16/2016', 'MM/DD/YYYY')
AND M.dateMatch>TO_DATE('06/15/2016', 'MM/DD/YYYY')
AND M.dateMatch<TO_DATE('06/17/2016', 'MM/DD/YYYY')
GROUP BY (J.numSecu, J.nom, J.prenom, J.equipe);


--
-- CLASSEMENT DES EQUIPES
--

-- Le classement des equipes tous championnats confondus
SELECT * FROM MVclassementEquipeTousChampionnats;

-- Le classement des equipes dans le championnat : FIFA World Cup 2016
SELECT * FROM MVclassementEquipeFIFAWorldCup2016;

-- Le classement des equipes les plus fortes à domicile tous championnats confondus
SELECT * FROM MVclassementEquipeAdomicile;

-- Le classement des equipes les plus fortes à l'exterieur tous championnats confondus
SELECT * FROM MVclassementEquipeAlexterieur;



--
-- Fonctionnalités : Niveau 2
--

--  | WORK IN PROGRESS | ----------------

-- Partie Utilisateur ----------------------------------------------------------------------------------------------------------------------------------------------------------------

-- 1) Les utilisateurs du site doivent pouvoir visualiser le nombre de buts de chaque équipe
SELECT * FROM vClassementNbButsFIFAWorldCup2016 WHERE equipe='France';
-- 2) Les utilisateurs du site doivent pouvoir visualiser les points des équipes

-- 3) Les utilisateurs du site doivent pouvoir visualiser le nombre de matchs gagnés

-- 4) Les utilisateurs du site doivent pouvoir visualiser le nombre de matchs ex aequo

-- 5) Les utilisateurs du site doivent pouvoir visualiser le nombre de matchs perdus

-- 6) Les utilisateurs du site doivent pouvoir visualiser la différence de but


-- Pour les joueurs de chaque équipe

-- 7) Les utilisateurs du site doivent pouvoir visualiser le nombre de fois où il a été sanctionné
----------------------- A modifier pour avoir les sanctions dans un championnat donnée ---------------------
SELECT count(S.type) AS NbSanction, nom, prenom
FROM Sanction S, Joueur J
WHERE S.numsecussanctionne =J.numSecu
AND M.nomChampionnat = 'FIFA World Cup'
AND M.dateChampionnat = TO_DATE('05/13/2012', 'MM/DD/YYYY')
GROUP BY J.numSecu;

-- 8) Les utilisateurs du site doivent pouvoir visualiser le nombre de buts qu’il a marqué
SELECT count(B.type) AS NbBut, J.nom as nom, J.prenom as prenom
FROM But B, Joueur J, Match M
WHERE B.numSecuButeur =J.numSecu
AND M.nomChampionnat = 'FIFA World Cup'
AND M.dateChampionnat = TO_DATE('05/13/2012', 'MM/DD/YYYY')
GROUP BY J.numSecu;
-- 9) Les utilisateurs du site doivent pouvoir visualiser le nombre de passes décisives
SELECT count(B.type) AS NbPasse, J.nom as nom, J.prenom as prenom
FROM But B, Joueur J, Match M
WHERE B.numSecuPasseur =J.numSecu
AND M.nomChampionnat = 'FIFA World Cup'
AND M.dateChampionnat = TO_DATE('05/13/2012', 'MM/DD/YYYY')
GROUP BY J.numSecu;
-- Pour les arbitres

-- 9) Les utilisateurs du site doivent pouvoir visualiser le nombre de sanction donné par un arbitre
SELECT count(S.type) AS NbSanction, A.nom as nom, A.prenom as prenom
FROM Sanction S, Arbitre A, Match M
WHERE M.arbitreCentral =A.numSecu
AND M.nomChampionnat = 'FIFA World Cup'
AND M.dateChampionnat = TO_DATE('05/13/2012', 'MM/DD/YYYY')
GROUP BY A.numSecu;
-- 10) Les utilisateurs du site doivent pouvoir visualiser le nombre de fois ou l'arbitre occupe un certain rôle

-- Pour les matchs

-- 11) Les utilisateurs du site doivent pouvoir visualiser le nombre de match dans une ville

-- 12) Les utilisateurs du site doivent pouvoir visualiser le match avec le plus d'événements

