#### NF17 - TD G3

# Contraintes à implémenter en dehors du SQL

## Personne

    - dateActuelle-dateDeNaissance > experience
    
## Match

    - PROJECTION(Championnat, nom) = PROJECTION(Match, championnat)
    - Les arbitres ne peuvent pas arbitrer deux matchs qui ont la même date.
    - Les équipes ne peuvent pas participer à deux matchs qui ont la même date.
