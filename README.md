# Projet_nf17_p19_tdg3_groupe4

# Projet de conception et de réalisation de la base de données du championnats de football

Projet en cours de réalisation dans le cadre de l'UV NF17

 - Cahier des charges fourni : https://librecours.net/projet/projets-19p-nx17/projets-19p-nx17_2.xhtml
 - Contraintes techniques : https://librecours.net/projet/projets-19p-nx17/cadre.xhtml

# Membres du groupe 
    - Clément Mouchet
    - Florent Gaudin
    - Xuefeng Hu
    - Zilu Zheng
    - Rémi Besoin
    
# Objet du projet

Le projet a pour but d'améliorer l'organisation d'un championnat de football à l'aide d'une gestion informatisée.
Le projet permettra d'avoir une visualisation détaillée des statistiques tels que les scores des matchs ou encore le meilleur buteur. 

# Fichiers liés au projet
    - Note de clarification (NDC)
    - Note de révision (NDR)
    - Modèle conceptuel des données (MCD)
    - Modèle Logique de Données (MLD)
    
# Utilisateurs principaux
    - Administrateur du championnat de football
    - Entraineurs, joueurs (consultation)